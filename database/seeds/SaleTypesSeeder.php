<?php

use Illuminate\Database\Seeder;

use \App\SaleType;

class SaleTypesSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$rows[] = ['id' => 1,'t_name' => 'GARANTIA',];
		$rows[] = ['id' => 2,'t_name' => 'PEDIDO INTERNO',];
		$rows[] = ['id' => 3,'t_name' => 'VENTA',];
		foreach ($rows as &$row) {
			($record = new SaleType($row))->save();
		}
	}
}
