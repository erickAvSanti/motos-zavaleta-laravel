<?php

use Illuminate\Database\Seeder;
use \App\MotorcyclePartsBrand;

class MotorcyclePartsBrandSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//
		$rows[] = ['id' => 1,'brand_name' => 'TVS',];
		$rows[] = ['id' => 2,'brand_name' => 'BAJAJ',];
  	foreach ($rows as &$row) {
  		($record = new MotorcyclePartsBrand($row))->save();
  	}
	}
}
