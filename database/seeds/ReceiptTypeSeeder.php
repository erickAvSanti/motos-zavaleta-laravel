<?php

use Illuminate\Database\Seeder;
use \App\ReceiptType;

class ReceiptTypeSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//
		$rows[] = ['id' => 1,'rt_name' => 'FACTURA',];
		$rows[] = ['id' => 2,'rt_name' => 'BOLETA',];
  	foreach ($rows as &$row) {
  		($record = new ReceiptType($row))->save();
  	}
	}
}
