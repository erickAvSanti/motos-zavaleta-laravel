<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMotorcyclePartsSupplierItemOnSalesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('motorcycle_parts_supplier_item_on_sales', function (Blueprint $table) {
			$table->id();
			$table->foreignId('supplier_item_id');
			$table->foreignId('brand_id');
			$table->string('item_currency')->default('S/');
			$table->decimal('public_price',9,2)->default(0);
			$table->decimal('public_price_discount',5,2)->default(30);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('motorcycle_parts_supplier_item_on_sales');
	}
}
