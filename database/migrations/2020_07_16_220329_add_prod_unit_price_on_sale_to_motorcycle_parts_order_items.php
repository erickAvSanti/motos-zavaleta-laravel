<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProdUnitPriceOnSaleToMotorcyclePartsOrderItems extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('motorcycle_parts_order_items', function (Blueprint $table) {
			//
			$table->decimal('prod_unit_price_on_sale',9,2)->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('motorcycle_parts_order_items', function (Blueprint $table) {
			//
			$table->dropColumn('prod_unit_price_on_sale');
		});
	}
}
