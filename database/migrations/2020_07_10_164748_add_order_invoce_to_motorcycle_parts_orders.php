<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrderInvoceToMotorcyclePartsOrders extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('motorcycle_parts_orders', function (Blueprint $table) {
			//
			$table->string('order_invoice',30)->default('')->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('motorcycle_parts_orders', function (Blueprint $table) {
			//
			$table->dropColumn('order_invoice');
		});
	}
}
