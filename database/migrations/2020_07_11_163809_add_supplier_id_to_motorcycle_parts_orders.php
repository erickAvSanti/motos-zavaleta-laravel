<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSupplierIdToMotorcyclePartsOrders extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('motorcycle_parts_orders', function (Blueprint $table) {
			//
			$table->foreignId('supplier_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('motorcycle_parts_orders', function (Blueprint $table) {
			//
			$table->dropColumn('supplier_id');
		});
	}
}
