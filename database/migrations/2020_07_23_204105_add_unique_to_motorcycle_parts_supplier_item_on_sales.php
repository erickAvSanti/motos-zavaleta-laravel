<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUniqueToMotorcyclePartsSupplierItemOnSales extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('motorcycle_parts_supplier_item_on_sales', function (Blueprint $table) {
			//
			$table->unique([
				'supplier_item_id', 
				'brand_id'],
				'unique_item_on_sale');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('motorcycle_parts_supplier_item_on_sales', function (Blueprint $table) {
			//
			$table->dropUnique('unique_item_on_sale');
		});
	}
}
