<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSaleTypeAndTechnicalStaffToSaleHistories extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sale_histories', function (Blueprint $table) {
			//
			$table->foreignId('sale_type_id');
			$table->foreignId('technical_staff_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sale_histories', function (Blueprint $table) {
			//
			$table->dropColumn('sale_type_id');
			$table->dropColumn('technical_staff_id');
		});
	}
}
