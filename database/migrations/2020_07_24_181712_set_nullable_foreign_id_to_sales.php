<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetNullableForeignIdToSales extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sales', function (Blueprint $table) {
			//
			$table->foreignId('sale_type_id')->nullable()->change();
			$table->foreignId('technical_staff_id')->nullable()->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sales', function (Blueprint $table) {
			//
			$table->foreignId('sale_type_id')->change();
			$table->foreignId('technical_staff_id')->change();
		});
	}
}
