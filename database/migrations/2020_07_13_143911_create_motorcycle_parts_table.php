<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMotorcyclePartsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('motorcycle_parts', function (Blueprint $table) {
			$table->id();
			$table->string('prod_name',100)->index();
			$table->string('prod_barcode',30)->unique();
			$table->decimal('prod_price',9,2)->default(0);
			$table->unsignedInteger('prod_stock')->default(0);
			$table->unsignedInteger('prod_stock_no_unit')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('motorcycle_parts');
	}
}
