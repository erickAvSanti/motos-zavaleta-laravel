<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMotorcyclePartsSupplierItemsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('motorcycle_parts_supplier_items', function (Blueprint $table) {
			$table->id();
			$table->string('item_code',30)->unique();
			$table->string('prod_name',100)->index();
			$table->string('item_unit',15)->index()->default('UND');
			$table->string('item_type',15)->index()->default('2R');
			$table->foreignId('motorcycle_model_id')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('motorcycle_parts_supplier_items');
	}
}
