<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrderReceiptTypeIdToMotorcyclePartsOrders extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('motorcycle_parts_orders', function (Blueprint $table) {
			//
			$table->foreignId('order_receipt_type_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('motorcycle_parts_orders', function (Blueprint $table) {
			//
			$table->dropColumn('order_receipt_type_id');
		});
	}
}
