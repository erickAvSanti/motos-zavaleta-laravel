<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetSupDocumentNullableToMotorcyclePartsSuppliers extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('motorcycle_parts_suppliers', function (Blueprint $table) {
			//
			$table->string('sup_document',25)->nullable()->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('motorcycle_parts_suppliers', function (Blueprint $table) {
			//
			$table->string('sup_document',25)->change();
		});
	}
}
