<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMotorcyclePartsOrderItemsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('motorcycle_parts_order_items', function (Blueprint $table) {
			$table->id();
			$table->string('order_item_code',30)->index();
			$table->string('prod_name',50);
			$table->string('prod_barcode',30)->nullable()->index();
			$table->unsignedInteger('order_item_quantity')->default(0);
			$table->decimal('prod_unit_price',9,2)->default(0);
			$table->decimal('order_item_percent_discount',9,2)->default(0);
			$table->decimal('order_item_total_price',9,2)->default(0);
			$table->foreignId('motorcycle_model_id');
			$table->foreignId('order_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('motorcycle_parts_order_items');
	}
}
