<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRoleUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_role_users', function (Blueprint $table) {
			$table->id();
			$table->foreignId('user_id');
			$table->boolean('list_records')->default(false);
			$table->boolean('view_record')->default(false);
			$table->boolean('update_record')->default(false);
			$table->boolean('create_record')->default(false);
			$table->boolean('delete_record')->default(false);
			$table->boolean('export_excel')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('user_role_users');
	}
}
