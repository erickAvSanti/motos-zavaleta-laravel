<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftDeletesToMotorcyclePartsOrderItems extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('motorcycle_parts_order_items', function (Blueprint $table) {
			//
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('motorcycle_parts_order_items', function (Blueprint $table) {
			//
			$table->dropSoftDeletes();
		});
	}
}
