<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeProdNameSizeToMotorcyclePartsOrderItems extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('motorcycle_parts_order_items', function (Blueprint $table) {
			//
			$table->string('prod_name',100)->index()->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('motorcycle_parts_order_items', function (Blueprint $table) {
			//
			$table->string('prod_name',50)->change();
		});
	}
}
