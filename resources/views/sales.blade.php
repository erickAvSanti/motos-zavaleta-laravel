@extends('layouts.app')

@section('content')
<!-- START MODAL  -->
@include('sales.modal_excel_report')
<!-- END MODAL  -->
<div class="card">
	<div class="card-header">
		<label>Salidas</label>
		<a href="{{ route('sales_new') }}">
			<button class="btn btn-primary btn-sm">Agregar Ventas</button>
		</a>
		<button 
			class="btn btn-success btn-sm" 
			data-toggle="modal" 
			data-target="#sales_report_excel_modal">
			Reportes <i class="fa fa-file-excel-o"></i>
		</button>
	</div>

	<div class="card-body">
		<form id="sales_form" method="GET" action="">
			<input type="hidden" name="order_by" value="{{ $order_by ?? '' }}">
			<input type="hidden" name="order" value="{{ $order ?? '' }}">
			<div class="form-check noselect">
				<input 
					class="form-check-input" 
					type="checkbox" 
					value="1" 
					name="show_deleted" 
					{{ isset($show_deleted) ? 'checked' : '' }} 
					id="show_deleted" />
				<label class="form-check-label" for="show_deleted">
					Mostrar eliminados
				</label>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-4 col-sm-12 card">
					<h6>Búsqueda por fecha de creación</h6>
					<div class="mb-2">
						<label>
							Desde 
							<div class="btn btn-sm btn-danger">
								<i class="fa fa-times"></i>
							</div>
						</label>
						<div class='input-group date' id='created_at_from'>
							<input 
								type='text' 
								class="form-control" 
								name="created_at_from" 
								value="{{ $created_at_from ?? '' }}" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-time"></span>
							</span>
						</div>
					</div>
					<div class="mb-2">
						<label>
							Hasta 
							<div class="btn btn-sm btn-danger">
								<i class="fa fa-times"></i>
							</div>
						</label>
						<div class='input-group date' id='created_at_to'>
							<input 
								type='text' 
								class="form-control" 
								name="created_at_to" 
								value="{{ $created_at_to ?? '' }}" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-time"></span>
							</span>
						</div>
					</div>
				</div>
			</div>
			<br />
			<div class="btn-group mb-1">
				<button 
					type="button" 
					class="btn btn-primary dropdown-toggle btn-sm" 
					data-toggle="dropdown" 
					aria-haspopup="true" 
					aria-expanded="false">
					{{ $rowsPerPage }} por página
				</button>
				<div class="dropdown-menu sales_rows_x_page">
					@foreach([20,50,100,150,200] as $rows_x_page)
					<a class="dropdown-item" rows_x_page="{{ $rows_x_page }}">{{ $rows_x_page }} por página</a>
					@endforeach
				</div>
			</div>
			<input type="hidden" name="page" value="1">
			<input type="hidden" name="rows_x_page" value="20">
			<nav aria-label="Paginación de búsqueda" class="nav_pagination">
				<ul class="pagination sales_pagination">
					<li class="page-item {{ $page == 1 ? 'disabled' : '' }}" page="{{ $page - 1 }}">
						<a 
							class="page-link" 
							tabindex="-1" 
							href="{{ $page == 1 ? '' : '#' }}"
							aria-disabled="{{ $page == 1 ? 'true' : 'false' }}">&laquo;</a>
					</li>
					@for($i = 1; $i <= $totalPages; $i++)
						@if($i != $page)
						<li class="page-item" page="{{ $i }}">
							<a class="page-link">{{$i}}</a>
						</li>
						@else
						<li 
							class="page-item active"
							aria-current="page">
							<a class="page-link">
								{{$i}}
								<span class="sr-only">(current)</span>
							</a>
						</li>
						@endif
					@endfor
					<li page="{{ $page + 1 }}"
						class="
							page-item 
							{{ $page == $totalPages ? 'disabled' : '' }} 
						" 
					>
						<a 
							class="page-link" 
							aria-disabled="{{ $page == $totalPages ? 'true' : 'false' }}" 
							href="{{ $page == $totalPages ? '' : '#' }}"
						>&raquo;</a>
					</li>
				</ul>
			</nav><label>Total: {{ $totalRows }}</label>
			<div class="input-group mb-3">
				<input 
					type="text" 
					class="form-control"  
					name="search" 
					value="{{ $search }}"
					placeholder="Buscar por producto">
				<div class="input-group-append">
					<button class="btn btn-sm btn-success">Buscar</button>
				</div>
			</div>
		</form>
		<div class="table-container2">
			<table 
				class="
					table 
					table-hover 
					table-borderer 
					table-stripped">
				<thead class="noselect">
					<tr>
						<th>
							<button 
								class="btn btn-sm btn-success" 
								refresh-action 
								refresh-for="#sales_form">
								<i class="fa fa-refresh"></i>
							</button>
						</th>
						<th>Productos</th>
						<th>Total</th>
						<th>Creado el</th>
						<th>Actualizado el</th>
						<th>Acción</th>
					</tr>
				</thead>
				<tbody>
					@foreach($records as $record_idx => $record)
						<tr>
							<td>{{ $rowsPerPage * ($page - 1) + $record_idx + 1 }}</td>
							<td>
								@php
									$products_list = json_decode($record->json_products,true);  
								@endphp
								<div class="sale-code">
									Código de venta: {{ $record->format_sale_code() }}
								</div>
								@if($record->sale_type)
								<div class="{{$record->sale_type->id != 3 ? 'mark-no-sale' : 'mark-normal-sale'}}">
									Tipo: {{ $record->sale_type->t_name }} {{ $record->issued_at ? ', Emitido: ' . ( is_string($record->issued_at) ? new \Carbon\Carbon($record->issued_at) : $record->issued_at)->format('d/m/Y') : '' }}
								</div>
								@endif
								<ul class="sale-list-items">
									@foreach($products_list AS &$product)
										@php
											$product_record = \App\MotorcyclePart::find($product['id']);
											$avg_purchase_price = null;
											if(isset($product['prod_purchase_price']) && \is_numeric($product['prod_purchase_price'])){
												$avg_purchase_price = $product['prod_purchase_price'];
											}else{
												if($product_record){
													$avg_purchase_price = $product_record->avg_price_from_orders_items_discounting_stock();
												}	
											}
										@endphp
										<li>
											{{ $product['prod_name'] }}, 
											({{ \App\Helpers\SaleHelper::get_prod_type_fullname($product) }} <b>{{ $product['prod_price'] }}</b>{{ $avg_purchase_price ? ", ".$avg_purchase_price : '' }}), 
											cantidad: <b>{{ $product['prod_quantity'] ?? 1 }}{{ \App\Helpers\SaleHelper::get_prod_type_shortname($product) }}</b>
											<div>Código: {{ $product['prod_barcode'] }}</div>
											<div class="sale-without-discount">Total: {{ sprintf('%0.2f',(float)$product['prod_quantity'] * (float)$product['prod_price'] * ( @$product['prod_by_weight'] == 'yes' ? 1/1000.0 : 1 )) }}</div>
											@if( !empty($product['discount_sale']) && $product['discount_sale'] == 'yes' )
											<div class="sale-with-discount">
												<i><strong>Vendido con otro precio por el total {{ sprintf('%0.2f',$product['discount_sale_price']) }}</strong></i>
											</div>
											@endif
										</li>
									@endforeach
								</ul>
							</td>
							<td>S/. {{ sprintf('%0.2f',$record->calc_total_values($products_list)) }}</td>
							<td>
								{{ 
									\Carbon\Carbon::parse($record->created_at)
									->setTimezone(env('APP_TIMEZONE'))
									->format('d/m/Y h:i:s a') 
								}}
							</td>
							<td>{{ \Carbon\Carbon::parse($record->updated_at)->setTimezone(env('APP_TIMEZONE'))->format('d/m/Y h:i:s a') }}</td>
							<td>
								<a href="{{ route('sales_show',$record->id) }}">
									<button class="mt-1 btn btn-sm btn-primary">Ver</button>
								</a>
								<button 
									class="mt-1 btn {{ isset($show_deleted) ? 'btn-warning' : 'btn-danger' }} btn-sm" 
									del-sale-action 
									record-code="{{ $record->format_sale_code() }}"
									record-id="{{ $record->id }}"  
									record-name="{{ $record->prod_name }}">
									{{ isset($show_deleted) ? 'Deshacer eliminación' : 'Eliminar' }}
								</button>
								@isset($record->deleted_at)
								<div>
									Eliminado el {{ $record->deleted_at->setTimeZone(env('APP_TIMEZONE'))->format('d/m/Y H:i:s a') }}
								</div>
								@endisset
								<form id="form_del_{{ $record->id }}" method="POST" action="{{ route('sales_delete',['id' => $record->id]) }}">
									@csrf
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
