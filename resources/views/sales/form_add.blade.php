@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>Nueva salida/venta</label>
		<a href="{{ route('sales') }}">
			<button class="btn btn-primary btn-sm">Ir a salidas/ventas</button>
		</a>
	</div>
	<div class="card-body">
		<div class="container">
			<div class="alert alert-warning" role="alert">
			  Formulario de nueva salida/venta
			</div>
			@foreach ($errors->all() as $error)
			<div class="alert alert-danger" role="alert">
				<i class="fa fa-times close-alert"></i>
			{{ $error }}
			</div>
			@endforeach
			<manage-product-items-on-sale 
				v-bind:is_new="true"
				v-bind:url_sales="'{{ route('sales') }}'" 
				v-bind:url_products="'{{ route('motorcycle_parts') }}'" 
				v-bind:url_technical_staff="'{{ route('technical_staff') }}'"
				v-bind:url_sale_types="'{{ route('sale_types') }}'"  
				v-bind:url_sales_create="'{{ route('sales_create') }}'" 
				v-bind:url_products_filter="'{{ route('motorcycle_parts_filter_by') }}'"></manage-product-items-on-sale>
		</div>
	</div>
</div>
@endsection
