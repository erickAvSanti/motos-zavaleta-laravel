@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>Visualización de salida/venta generada</label>
		<a href="{{ route('sales') }}">
			<button class="btn btn-primary btn-sm">Ir a salidas/ventas</button>
		</a>
		<a href="{{ route('sales_new') }}">
			<button class="btn btn-primary btn-sm">Agregar nueva salida/venta</button>
		</a>
	</div>
	<div class="card-body">
		<div class="container">
			@if(!isset($record))
				<div class="alert alert-danger" role="alert">
					Registro de venta no encontrada con el identificador: {{ $id }}
				</div>
			@else
				<div class="alert alert-success" role="alert">
				  Visualización de registro de salida/venta generada: {{ $record->format_sale_code() }}
				</div>
				@foreach ($errors->all() as $error)
				<div class="alert alert-danger" role="alert">
					<i class="fa fa-times close-alert"></i>
				{{ $error }}
				</div>
				@endforeach
				<manage-product-items-on-sale 
					v-bind:is_new="false"
					v-bind:current_sale="{{ $record ? $record->toJson() : 'null' }}"
					v-bind:url_sales="'{{ route('sales') }}'" 
					v-bind:url_sales_printing_mode="'{{ route('sales_printing_mode',$record->id) }}'" 
					v-bind:url_products="'{{ route('motorcycle_parts') }}'" 
					v-bind:url_technical_staff="'{{ route('technical_staff') }}'" 
					v-bind:url_sale_types="'{{ route('sale_types') }}'" 
					v-bind:url_sales_create="'{{ route('sales_create') }}'" 
					v-bind:url_products_filter="'{{ route('motorcycle_parts_filter_by') }}'"></manage-product-items-on-sale>
				<hr />
				<div>
					<h5>Historial de modificaciones de esta salida/venta</h5>
					<table class="table table-hover table-boderer table-stripped">
						<thead>
							<tr>
								<th>#</th>
								<th>Total</th>
								<th>Productos</th>
								<th>Creado el</th>
							</tr>
						</thead>
						@if(count($histories)>0)
						<tbody>
							@foreach ($histories as $history_idx => $history)
								<tr>
									<td>{{ ($history_idx + 1) }}</td>
									<td>{{ $history->sale_total }}</td>
									<td>
										@php
											$history_products = json_decode($history->json_products,true);	
										@endphp
										<table class="table-primary">
											<tr>
												<th>#</th>
												<th>Producto</th>
												<th>Precio <br />Unidad/<br />Kilo/<br />Litro</th>
												<th>Es por peso?</th>
												<th>Cantidad</th>
											</tr>
											@foreach( $history_products AS $his_prod_idx => $history_product )
											<tr>
												<td>
													<span>{{ $his_prod_idx + 1 }}</span>
												</td>
												<td>
													<strong>{{ $history_product['prod_name'] }}</strong><br />
													<i>{{ $history_product['prod_barcode'] }}</i>
												</td>
												@if(@$history_product['prod_by_weight'] == 'no')
												<td>S/. {{ $history_product['prod_price'] }}</td>
												<td>NO</td>
												<td>{{ @$history_product['prod_quantity'] ?? 0 }}</td>
												@else
												<td>S/. {{ sprintf('%0.2f',@$history_product['prod_price']) }}</td>
												<td>SI</td>
												<td>{{ @$history_product['prod_quantity'] ?? 0}}{{ @$history_product['prod_weight_type'] }}</td>
												@endif
											</tr>
											@endforeach
										</table>
										@isset( $history->sale_type )
										<div>
											Tipo: {{ $history->sale_type->t_name ?? '' }}
										</div>
										@endisset
										@isset( $history->technical_staff )
										<div>
											Técnico: {{ $history->technical_staff->staff_name ?? '' }}
										</div>
										@endisset
										@if( $history->issued_at)
										<div>
											Emitido el: {{ \Carbon\Carbon::createFromFormat("!Y-m-d",$history->issued_at)->format('d/m/Y') }}
										</div>
										@endif
									</td>
									<td>{{ $history->created_at->setTimezone('America/Lima')->format('d/m/Y H:i:s a') }}</td>
								</tr>
							@endforeach
						</tbody>
						@else
						<tbody>
							<tr>
								<td colspan=4>Sin registro histórico</td>
							</tr>
						</tbody>
						@endif
					</table>
				</div>
			@endif
		</div>
	</div>
</div>
@endsection
