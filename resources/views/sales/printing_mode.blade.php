<style type="text/css">
	@page { size: auto;  margin: 0mm; }
	*{
		font-family: Arial, Helvetica, sans-serif;
	}
	table{
		width: 100%;
	}
	table,.dates{
		font-size: 11px;
	}
	.center{
		text-align: center;
	}
	.hh{
		margin-top: 5px;
		margin-bottom: 5px;
	}
</style>
@php
	$products_arr = json_decode($record->json_products,true);
@endphp
<h3 class="center hh">Repuestos Zavaleta S.A.C.</h3>
<h5 class="center hh">Registro de pedidos</h5>
<h5 class="center hh">{{ $record->format_sale_code() }}</h5>
@if(is_array($products_arr))
	<table>
		<tr>
			<th>#</th>
			<th style="text-align: left;">Producto</th>
			<th style="text-align: left;">Cantidad</th>
			<th style="text-align: left;">Prec.<br />Unidad/<br />kilo/<br />Litro.</th>
			<th style="text-align: left;">Prec.<br />Total</th>
		</tr>

		@php
		$total_price = 0.0;
		@endphp
		@foreach($products_arr AS $product_index => $product)
		@php
			if(empty(@$product['discount_sale']) || @$product['discount_sale'] == 'no')
			{
				if($product['prod_by_weight'] == 'yes'){
					$total_by_product = @$product['prod_quantity'] * ( @$product['prod_price'] / 1000.0 );
				}else{
					$total_by_product = @$product['prod_quantity'] * @$product['prod_price'];
				}
			}else{
				$total_by_product = (float)@$product['discount_sale_price'];
				$prod_unit_price_with_discount = $total_by_product / (float)$product['prod_quantity'];
			}
			$total_price += $total_by_product;
		@endphp
		<tr>
			<td>{{ str_pad($product_index + 1, 2, '0', STR_PAD_LEFT) }})</td>
			<td>
				<div>
					{{ $product['prod_name'] }}.
				</div>
				<div>
					{{ $product['prod_barcode'] }}
				</div>
			</td>
			<td>{{ $product['prod_quantity'] }}{{ $product['prod_by_weight'] == 'yes' ? ( $product['prod_weight_type'] ) : ' u' }}</td>
			<td>
				@if(isset($prod_unit_price_with_discount))
					<div>*S/. {{ round($prod_unit_price_with_discount,2) }}</div>	
				@else 
					<div>S/. {{ $product['prod_price'] }}</div>
				@endif
			</td>
			<td>S/. {{ sprintf("%.2f",$total_by_product) }}</td>
		</tr>
		@endforeach
	</table>
	<div style="text-align: center;">
		<h5>Total: S/. {{ sprintf("%.2f",$total_price) }}</h5>
	</div>
	<div class="dates">
		(*) Precio unitario referencial
	</div>
	<div class="dates">
		Fecha y hora de impresión: <div style="display:inline-block;">{{ \Carbon\Carbon::now()->setTimezone(env('APP_TIMEZONE'))->format('d/m/Y H:i:s a') }}</div>
	</div>
	<div class="dates">
		Fecha y hora de registro: <div style="display:inline-block;">{{ $record->created_at->setTimezone(env('APP_TIMEZONE'))->format('d/m/Y H:i:s a') }}</div>
	</div>
@endif