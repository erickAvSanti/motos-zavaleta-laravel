<div 
	class="modal" 
	id="sales_report_excel_modal" 
	tabindex="-1" 
	role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Reportes excel</h5>
        <button 
        	type="button" 
        	class="close" 
        	data-dismiss="modal" 
        	aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <div class="card bg-light mt-1">
          <div class="card-header">Del día</div>
          <div class="card-body">
            <a 
              href="{{ route('sales_excel_report',['by'=>'sales_of_the_day']) }}" target="_blank">
              <button class="btn btn-success btn-success btn-sm">
                <span>Exportar ventas del día</span>
                <i class=" fa fa-file-excel-o"></i>
              </button>
            </a>
          </div>
        </div>
        <div class="card bg-light mt-1">
          <div class="card-header">De días específicos</div>
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6 col-sm-12">
                <label>Desde</label>
                <div class='input-group date' id='excel_report_from_date'>
                  <input type='text' class="form-control" />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-time"></span>
                  </span>
                </div>
              </div>
              <div class="col-lg-6 col-sm-12">
                <label>Hasta</label>
                <div class='input-group date' id='excel_report_to_date'>
                  <input type='text' class="form-control" />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-time"></span>
                  </span>
                </div>
              </div>
            </div>
            <button 
              id="excel_export_from_dates" 
              class="btn btn-sm btn-success mt-1">
              <span>Exportar</span>
              <i class=" fa fa-file-excel-o"></i>
            </button>
          </div>
        </div>
        <div class="card bg-light mt-1">
          <div class="card-header">De ID de tickets específicos</div>
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6 col-sm-12">
                <label>Desde</label>
                <div class="form-group">
                  <input 
                    type="number" 
                    class="form-control" 
                    id="excel_export_from_id" 
                    aria-describedby="Desde ID Ticket">
                </div>
              </div>
              <div class="col-lg-6 col-sm-12">
                <label>Hasta</label>
                <div class="form-group">
                  <input 
                    type="number" 
                    class="form-control" 
                    id="excel_export_to_id" 
                    aria-describedby="Hasta ID Ticket">
                </div>
              </div>
            </div>
            <button 
              id="sales_excel_export_from_ids" 
              class="btn btn-sm btn-success mt-1">
              <span>Exportar</span>
              <i class=" fa fa-file-excel-o"></i>
            </button>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button 
        	type="button" 
        	class="btn btn-secondary" 
        	data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>