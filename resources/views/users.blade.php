@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>Usuarios {{ isset($show_deleted) ? 'eliminados' : '' }}</label>
		<a href="{{ route('users_new') }}">
			<button class="btn btn-primary btn-sm">
				<i class="fa fa-plus"></i>
			</button>
		</a>
	</div>

	<div class="card-body">
		<form id="users_form" method="GET" action="">
			<input type="hidden" name="order_by" value="{{ $order_by ?? '' }}">
			<input type="hidden" name="order" value="{{ $order ?? '' }}">
			<div class="form-check noselect">
			  <input 
			  	class="form-check-input" 
			  	type="checkbox" 
			  	value="1" 
			  	name="show_deleted" 
			  	{{ isset($show_deleted) ? 'checked' : '' }} 
			  	id="show_deleted" />
			  <label class="form-check-label" for="show_deleted">
			    Mostrar eliminados
			  </label>
			</div>
		</form>
		<div class="table-container2">
			<table class="table table-hover table-borderer table-stripped">
				<thead class="noselect">
					<tr>
						<th>
							<button 
								class="btn btn-sm btn-success" 
								refresh-action 
								refresh-for="#users_form">
								<i class="fa fa-refresh"></i>
							</button>
						</th>
						<th>Usuario</th>
						<th>Email</th>
						<th>Es admin?</th>
						<th>Acción</th>
					</tr>
				</thead>
				<tbody>
					@foreach($records as $record_idx => $record)
						<tr>
							<td>{{ $record_idx + 1 }}</td>
							<td>{{ $record->name }}</td>
							<td>{{ $record->email }}</td>
							<td>{{ $record->is_admin == 1 ? 'SI' : 'NO' }}</td>
							<td>
								<a href="{{ route('users_show',$record->id) }}">
									<button class="mt-1 btn btn-sm btn-primary">Ver</button>
								</a>
								<button 
									class="mt-1 btn {{ isset($show_deleted) ? 'btn-warning' : 'btn-danger' }} btn-sm" 
									del-user-action 
									form-del="#form_del_{{ $record->id }}"
									record-id="{{ $record->id }}"  
									record-name="{{ $record->prod_name }}">
									{{ isset($show_deleted) ? 'Deshacer eliminación' : 'Eliminar' }}
								</button>
								<form 
									id="form_del_{{ $record->id }}" 
									method="POST" 
									action="{{ route('users_delete',$record->id) }}">
									@csrf
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
