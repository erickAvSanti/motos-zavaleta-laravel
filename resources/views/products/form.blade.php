				<input type="hidden" name="from" value="ui">
				@csrf
				<div class="form-group row">
					<label 
						for="prod_name" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">Producto</label>
					<div class="col-sm-10">
						<input 
							type="text" 
							class="
								form-control 
								form-control-md" 
							id="prod_name" 
							name="prod_name" 
							value="{{ isset($record) ? $record->prod_name : old('prod_name') }}"
							placeholder="El nombre del producto" />
					</div>
				</div>
				<div class="form-group row">
					<label 
						for="prod_barcode" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">Código de Barra</label>
					<div class="col-sm-10">
						<input 
							type="text" 
							class="
								form-control 
								form-control-md" 
							id="prod_barcode" 
							name="prod_barcode" 
							value="{{ isset($record) ? $record->prod_barcode : old('prod_barcode') }}"
							placeholder="El código de barra del producto" />
					</div>
				</div>
				<div class="form-group row">
					<label 
						for="prod_price" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">Precio de venta al público</label>
					<div class="col-sm-10">
						<input 
							type="text" 
							class="
								form-control 
								form-control-md" 
							id="prod_price" 
							name="prod_price" 
							value="{{ isset($record) ? $record->prod_price : old('prod_price') }}"
							placeholder="Ej 23.60" />
					</div>
				</div>
				<div class="form-group row">
					<label 
						for="prod_stock" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">Stock</label>
					<div class="col-sm-10">
						<input 
							type="text" 
							class="
								form-control 
								form-control-md" 
							id="prod_stock" 
							name="prod_stock" 
							value="{{ isset($record) ? $record->prod_stock : old('prod_stock') }}"
							placeholder="Ej 5" />
					</div>
				</div>
				<div class="form-group row">
					<label 
						for="prod_stock_no_unit" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">Stock restante en gramos o mililitros</label>
					<div class="col-sm-10">
						<input 
							type="number" 
							placeholder="Ej. 350" 
							class="
								form-control 
								form-control-md" 
							id="prod_stock_no_unit" 
							name="prod_stock_no_unit" 
							value="{{ isset($record) ? $record->prod_stock_no_unit : old('prod_stock_no_unit') }}"
							placeholder="Ej 5" />
					</div>
				</div>