@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>PRODUCTO CREADO</label>
		<a href="{{ route('products') }}">
			<button class="btn btn-primary btn-sm">Ir a productos</button>
		</a>
	</div>

	<div class="card-body">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					@include('products.preview_info')
				</div>
				<div class="col-lg-6">
					@include('products.show_list_suppliers_for_this_product')
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
