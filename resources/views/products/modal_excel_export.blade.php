<div 
  class="modal" 
  id="modal_excel_export" 
  tabindex="-1" 
  role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Reportes excel</h5>
        <button 
          type="button" 
          class="close" 
          data-dismiss="modal" 
          aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <a href="{{ route('motorcycle_parts_excel_export_all') }}" target="_blank">
          Exportar todo
        </a>
      </div>
      <div class="modal-footer">
        <button 
          type="button" 
          class="btn btn-secondary" 
          data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>