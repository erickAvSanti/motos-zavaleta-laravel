@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>PRODUCTO ACTUALIZADO</label>
	</div>

	<div class="card-body">
		<div class="container">
			<label>Producto: {{ $record->prod_name }}</label>
			<div>
				<ul>
					<li>
						<label><strong>Producto:</strong></label>
						<span>{{ $record->prod_name }}</span>
					</li>
					<li>
						<label><strong>Código de Barra:</strong></label>
						<span>{{ $record->prod_barcode }}</span>
					</li>
					<li>
						<label><strong>Precio de venta:</strong></label>
						<span>{{ $record->prod_price }}</span>
					</li>
					<li>
						<label><strong>Stock:</strong></label>
						<span>{{ $record->prod_stock }}</span>
					</li>
					<li>
						<label><strong>Stock Restante:</strong></label>
						<span>{{ $record->prod_stock_no_unit }}</span>
					</li>
					<li>
						<label><strong>Descripción:</strong></label><br />
						<div>{{ $record->prod_desc ? $record->prod_desc : '-' }}</div>
					</li>
				</ul>
			</div>
			<a href="{{ route('motorcycle_parts_show',$record->id) }}">
				<button class="btn btn-primary btn-sm">Editar Producto</button>
			</a>
			<a href="{{ route('motorcycle_parts') }}">
				<button class="btn btn-primary btn-sm">Ir a productos</button>
			</a>
		</div>
	</div>
</div>
@endsection
