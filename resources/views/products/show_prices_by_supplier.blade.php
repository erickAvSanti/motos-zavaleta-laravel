@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		@if(isset($supplier) && isset($product))
		<label>Registro de precios de compra para el proveedor <strong>{{ strtoupper($supplier->sup_name) }}</strong></label>
		@else
		<label>El producto y/o el proveedor no existe(n).</label>
		@endif
	</div>

	<div class="card-body">
		<div class="container">
			@if(isset($supplier) && isset($product))
			<div>Información del producto</div>
			@php
				$record = $product;	
			@endphp
			@include('products.preview_info')
			<manage-product-prices-by-supplier 
				v-bind:url_purchases="'{{ route( 'product_suppliers_purchases' ) }}'" 
				v-bind:url_list_purchases="'{{ route( 'product_suppliers_list_purchases',['product_id' => $product->id, 'supplier_id' => $supplier->id] ) }}'" 
				v-bind:url_product_suppliers_purchase="'{{ route('product_suppliers_purchase_create') }}'"
				v-bind:current_product="{{ $product->toJson() }}"
				v-bind:current_supplier="{{ $supplier->toJson() }}"
			></manage-product-prices-by-supplier>
			@endif
		</div>
	</div>
</div>
@endsection
