
<div class="preview_info">
	<ul>
		<li>
			<label><strong>Producto:</strong></label>
			<span>{{ $record->prod_name }}</span>
		</li>
		<li>
			<label><strong>Código de Barra:</strong></label>
			<span>{{ $record->prod_barcode }}</span>
		</li>
		<li>
			<label><strong>Precio de venta:</strong></label>
			<span>{{ $record->prod_price }}</span>
		</li>
		<li>
			<label><strong>Descripción:</strong></label><br />
			<div>{{ $record->prod_desc ? $record->prod_desc : '-' }}</div>
		</li>
	</ul>
	<a href="{{ route('motorcycle_parts_show',$record->id) }}">
		<button class="btn btn-outline-primary btn-sm">Modificar producto</button>
	</a>
</div>