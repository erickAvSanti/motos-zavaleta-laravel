				<input type="hidden" name="from" value="ui">
				@csrf
				<div class="form-group row">
					<label 
						for="staff_name" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">Personal técnico</label>
					<div class="col-sm-10">
						<input 
							type="text" 
							class="
								form-control 
								form-control-md" 
							id="staff_name" 
							name="staff_name" 
							value="{{ isset($record) ? $record->staff_name : old('staff_name') }}"
							placeholder="El nombre del personal técnico" />
					</div>
				</div>