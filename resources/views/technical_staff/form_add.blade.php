@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>Nuevo producto</label>
		<a href="{{ route('technical_staff') }}">
			<button class="btn btn-primary btn-sm">Ir a personal técnico</button>
		</a>
	</div>
	<div class="card-body">
		<div class="container">
			@foreach ($errors->all() as $error)
			<div class="alert alert-danger" role="alert">
				<i class="fa fa-times close-alert"></i>
			{{ $error }}
			</div>
			@endforeach
			<form id="technical_staff_form_create" method="POST" action="{{ route('technical_staff_create') }}">
				@include('technical_staff.form')
				<button class="btn btn-primary btn-sm" type="submit">Guardar</button>
			</form>
			<hr />
			<label class="required_asterisk">Campo obligatorio</label>
		</div>
	</div>
</div>
@endsection
