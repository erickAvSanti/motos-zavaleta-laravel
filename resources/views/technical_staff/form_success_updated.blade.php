@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>PERSONAL TÉCNICO ACTUALIZADO</label>
	</div>

	<div class="card-body">
		<div class="container">
			<label>Personal técnico: {{ $record->staff_name }}</label>
			<div>
				<ul>
					<li>
						<label><strong>Personal técnico:</strong></label>
						<span>{{ $record->staff_name }}</span>
					</li>
				</ul>
			</div>
			<a href="{{ route('technical_staff_show',$record->id) }}">
				<button class="btn btn-primary btn-sm">Editar personal técnico</button>
			</a>
			<a href="{{ route('technical_staff') }}">
				<button class="btn btn-primary btn-sm">Ir al listado de personal técnico</button>
			</a>
		</div>
	</div>
</div>
@endsection
