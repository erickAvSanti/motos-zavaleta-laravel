@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		@if($is_trashed)
		<label>
			{{ $record ? ( $deleted ? 'Registro de personal técnico restaurado' : 'No se pudo restaurar el registro de personal técnico' ) : 'El personal técnico no existe' }}
		</label>
		@else
		<label>
			{{ $record ? ( $deleted ? 'Registro de personal técnico eliminado' : 'No se pudo eliminar el registro de personal técnico' ) : 'El personal técnico no existe' }}
		</label>
		@endif
	</div>

	<div class="card-body">
		<div class="container">
			@if($record && $deleted)
			<label>Registro de personal técnico {{ $is_trashed ? 'restaurado' : 'eliminado' }}: {{ $record->staff_name }}</label><br />
			@endif
			<a href="{{ route('technical_staff') }}">
				<button class="btn btn-primary btn-sm">Regresar al listado de personal técnico</button>
			</a>
		</div>
	</div>
</div>
@endsection
