@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>
			{{ $record ? ( "Personal técnico: " . $record->staff_name ) : 'Personal no encontrado' }}
		</label>
	</div>
	<div class="card-body">
		<div class="container">
			@if( isset($record) )
				@foreach ($errors->all() as $error)
				<div class="alert alert-danger" role="alert">
					<i class="fa fa-times close-alert"></i>
				{{ $error }}
				</div>
				@endforeach
				@if(isset($record))
					<a href="{{ route('technical_staff') }}">
						Regresar al listado de personal técnico
					</a>
				@endif
				<form id="technical_staff_form_update" method="POST" action="{{ route('technical_staff_update',$record->id) }}">
					@include('technical_staff.form')
				</form>
				<button 
					class="
						btn 
						btn-primary 
						btn-sm 
						btn-technical-staff-update">Modificar</button>
				<button 
					class="btn btn-danger btn-sm" 
					mod-name="product"
					del-action 
					record-id="{{ $record->id }}" 
					record-name="{{ $record->staff_name }}" 

					>
					Eliminar
				</button>
				<form 
					id="form_del_{{ $record->id }}" 
					method="POST" 
					action="{{ route('technical_staff_delete',$record->id) }}">
					@csrf
				</form>
				<hr />
				<label class="required_asterisk">Campo obligatorio</label>
			@else
				<a href="{{ route('technical_staff') }}">
					Regresar al listado de personal técnico
				</a>
			@endif
		</div>
	</div>
</div>
@endsection
