
<div class="preview_info">
	<ul>
		<li>
			<label><strong>Personal técnico:</strong></label>
			<span>{{ $record->staff_name }}</span>
		</li>
	</ul>
	<a href="{{ route('technical_staff_show',$record->id) }}">
		<button 
			class="
				btn 
				btn-outline-primary 
				btn-sm">Modificar personal técnico</button>
	</a>
</div>