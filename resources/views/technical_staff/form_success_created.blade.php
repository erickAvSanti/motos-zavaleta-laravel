@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>PERSONAL CREADO</label>
		<a href="{{ route('technical_staff') }}">
			<button class="btn btn-primary btn-sm">Ir a personal técnico</button>
		</a>
	</div>

	<div class="card-body">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					@include('technical_staff.preview_info')
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
