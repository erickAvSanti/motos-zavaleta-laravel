@extends('layouts.app')

@section('content')
<manage-motorcycle-parts-supplier-item-on-sales
	v-bind:url_server="'{{route('motorcycle_parts_supplier_item_on_sales')}}'"
	v-bind:url_server_brands="'{{route('motorcycle_parts_brands')}}'"
	v-bind:url_server_moto_models="'{{ route('motorcycle_models') }}'"
></manage-motorcycle-parts-supplier-item-on-sales>
@endsection
