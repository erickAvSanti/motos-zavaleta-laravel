@extends('layouts.app')

@section('content')
<manage-motorcycle-parts-orders 
	v-bind:server_url="'{{ route('motorcycle_parts_orders') }}'"
	v-bind:server_url_moto_models="'{{ route('motorcycle_models') }}'"
	v-bind:server_url_suppliers="'{{ route('motorcycle_part_suppliers') }}'"
	v-bind:server_url_receipt_types="'{{ route('receipt_types') }}'"
	v-bind:server_url_motorcycle_parts="'{{ route('motorcycle_parts') }}'"
></manage-motorcycle-parts-orders>
@endsection
