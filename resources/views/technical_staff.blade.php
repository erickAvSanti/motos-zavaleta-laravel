@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>
			Personal Técnico 
			<a href="{{route('technical_staff_new')}}">
				<button class="btn btn-success btn-sm">
					<i class="fa fa-plus"></i>
				</button>
			</a>
		</label>
	</div>

	<div class="card-body">
		<form id="technical_staff_form" method="GET" action="">
			<input type="hidden" name="order_by" value="{{ $order_by ?? '' }}">
			<input type="hidden" name="order" value="{{ $order ?? '' }}">
			<div class="input-group mb-3">
			  <div class="input-group-prepend">
			    <span class="input-group-text">
			    	<i class="fa fa-search"></i>
			    </span>
			  </div>
			  <input 
			  	type="text" 
			  	class="form-control" 
			  	name="query_value"
			  	value="{{ $query_value ?? ''}}"
			  	placeholder="Búsqueda" 
			  	aria-label="Búsqueda de personal técnico" 
			  	aria-describedby="basic-technical-staff-search" />
				  <div class="input-group-append">
				    <button 
				    	class="btn btn-success" 
				    	id="basic-technical-staff-search">
				    	Buscar
				    </button>
				  </div>
			</div>
			<div class="form-check noselect">
			  <input 
			  	class="form-check-input" 
			  	type="checkbox" 
			  	value="1" 
			  	name="show_deleted" 
			  	{{ isset($show_deleted) ? 'checked' : '' }} 
			  	id="show_deleted" />
			  <label class="form-check-label" for="show_deleted">
			    Buscar en eliminados
			  </label>
			</div>
			<br />
			<div class="btn-group mb-1">
				<button 
					type="button" 
					class="btn btn-primary dropdown-toggle btn-sm" 
					data-toggle="dropdown" 
					aria-haspopup="true" 
					aria-expanded="false">
					 {{ $rowsPerPage }} por página
				</button>
				<div class="dropdown-menu technical_staff_rows_x_page">
					@foreach([20,50,100,150,200] as $rows_x_page)
					<a class="dropdown-item" rows_x_page="{{ $rows_x_page }}">{{ $rows_x_page }} por página</a>
					@endforeach
				</div>
			</div>
			<input type="hidden" name="page" value="1">
			<input type="hidden" name="rows_x_page" value="{{$rowsPerPage}}">
			<nav aria-label="Paginación de búsqueda" class="nav_pagination">
				<ul class="pagination technical_staff_pagination">
					<li 
						class="page-item {{ $page == 1 ? 'disabled' : '' }}" 
						page="{{ $page - 1 }}">
						<a 
							class="page-link" 
							tabindex="-1" 
							href="{{ $page == 1 ? '' : '#' }}"
							aria-disabled="{{ $page == 1 ? 'true' : 'false' }}">&laquo;</a>
					</li>
					@for($i = 1; $i <= $totalPages; $i++)
						@if($i != $page)
						<li class="page-item" page="{{ $i }}">
							<a class="page-link">{{$i}}</a>
						</li>
						@else
						<li 
							class="page-item active"
							aria-current="page">
							<a class="page-link">
								{{$i}}
								<span class="sr-only">(current)</span>
							</a>
						</li>
						@endif
					@endfor
					<li page="{{ $page + 1 }}"
						class="
							page-item 
							{{ $page == $totalPages ? 'disabled' : '' }} 
						" 
					>
						<a 
							class="page-link" 
							aria-disabled="{{ $page == $totalPages ? 'true' : 'false' }}" 
							href="{{ $page == $totalPages ? '' : '#' }}"
						>&raquo;</a>
					</li>
				</ul>
			</nav>
			<div class="alert alert-success" role="alert">
				Total: {{ $totalRows }}
			</div>
		</form>
		<div class="table-container2">
			<table 
				class="
					table 
					table-hover 
					table-borderer 
					table-stripped">
				<thead class="noselect">
					<tr>
						<th>
							<button 
								class="btn btn-success btn-sm" 
								id="reload-technical-staff-table">
								<i class="fa fa-refresh"></i>
							</button>
						</th>
						<th 
							form_tag_id="#technical_staff_form" 
							order_by="staff_name" 
							order="{{ $order_by == 'staff_name' ? $order : '' }}">Personal Técnico</th>
						<th>Acción</th>
					</tr>
				</thead>
				<tbody>
					@foreach($records as $idx => $record)
						<tr>
							<td>{{ $rowsPerPage * ($page - 1) + $idx + 1 }}</td>
							<td>
								{{ $record->staff_name }}
							</td>
							<td>
								@if( empty($record->deleted_at) )
								<a href="{{ route('technical_staff_show',['id' => $record->id]) }}">
									<button class="btn btn-primary btn-sm mb-1">
										Ver
									</button>
								</a>
								@endif
								<button 
									class="btn {{ isset($show_deleted) ? 'btn-warning' : 'btn-danger' }} btn-sm mb-1" 
									mod-name="technical_staff" 
									del-action 
									record-id="{{ $record->id }}"  
									record-name="{{ $record->staff_name }}">
									<i class="fa fa-trash"></i>
								</button>
								<form 
									id="form_del_{{ $record->id }}" 
									method="POST" 
									action="{{ route('technical_staff_delete',['id' => $record->id]) }}">
									@csrf
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
