@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>USUARIO {{ !isset($record) ? 'NO': '' }} ACTUALIZADO</label>
		@unless(isset($record))
		<a href="{{ route('users') }}">
			<button class="btn btn-primary btn-sm">Ir a usuarios</button>
		</a>
		@endunless
	</div>

	@isset($record)
	<div class="card-body">
		<div class="container">
			<div>
				<ul>
					<li>
						<label><strong>Usuario:</strong></label>
						<span>{{ $record->name }}</span>
					</li>
					<li>
						<label><strong>Email:</strong></label>
						<span>{{ $record->email }}</span>
					</li>
					<li>
						<label><strong>Administrador:</strong></label>
						<span>{{ $record->is_admin == 1 ? 'SI' : 'NO' }}</span>
					</li>
				</ul>
			</div>
			<a href="{{ route('users_show',$record->id) }}">
				<button class="btn btn-primary btn-sm">Editar usuario</button>
			</a>
			<a href="{{ route('users') }}">
				<button class="btn btn-primary btn-sm">Ir a usuarios</button>
			</a>
			<a href="{{ route('users_new') }}">
				<button class="btn btn-success btn-sm">Crear otro usuario</button>
			</a>
		</div>
	</div>
	@endisset
</div>
@endsection
