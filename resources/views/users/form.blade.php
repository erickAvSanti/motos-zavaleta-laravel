				<input type="hidden" name="from" value="ui">
				@csrf
				<div class="form-group row">
					<label 
						for="name" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">Nombre</label>
					<div class="col-sm-10">
						<input 
							type="text" 
							class="
								form-control 
								form-control-md" 
							id="name" 
							name="name" 
							value="{{ isset($record) ? $record->name : old('name') }}"
							placeholder="Usuario" />
					</div>
				</div>
				<div class="form-group row">
					<label 
						for="email" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">Email</label>
					<div class="col-sm-10">
						<input 
							type="text" 
							class="
								form-control 
								form-control-md" 
							id="email" 
							name="email" 
							value="{{ isset($record) ? $record->email : old('email') }}"
							placeholder="Email" />
					</div>
				</div>
				<div class="form-group row">
					<label 
						for="user_password" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">Contraseña</label>
					<div class="col-sm-10">
						<input 
							type="password" 
							class="
								form-control 
								form-control-md" 
							id="password" 
							name="password" 
							value="{{ old('password') }}"
							placeholder="Contraseña" />
					</div>
				</div>
				<div class="form-group row">
					<label 
						for="password_confirmation" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">Confirmar contraseña</label>
					<div class="col-sm-10">
						<input 
							type="password" 
							class="
								form-control 
								form-control-md" 
							id="password_confirmation" 
							name="password_confirmation" 
							value="{{ old('password_confirmation') }}"
							placeholder="Contraseña" />
					</div>
				</div>
				<div class="form-group row">
					<div class="form-check">
					  <input 
					  	class="form-check-input" 
					  	type="checkbox" 
							value="1" 
							name="is_admin"
							{{ (isset($record) && $record->is_admin == 1) || old('is_admin') == 1 ? 'checked' : '' }}
					  	id="is_admin">
					  <label 
					  	class="form-check-label" 
					  	for="is_admin">
					    Es administrador?
					  </label>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<div class="card m-2">
							<div class="card-header">
								<h5>Permisos en usuarios</h5>
							</div>
							<div class="card-body">
								@foreach ( \App\UserRoleUser::CASTS_DESCRIPTION as $key => $desc)
									
									<div class="form-group row">
										<div class="form-check noselect">
											<input 
												class="form-check-input" 
												type="checkbox" 
												name="role_user__{{$key}}" 
												id="role_user__{{$key}}" 
												value="1" 
												{{ ( isset($record) && $record->hasRole("user__$key") ) ? 'checked' : '' }}>
											<label 
												class="form-check-label" 
												for="role_user__{{$key}}">
												{{ $desc }}
											</label>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="card m-2">
							<div class="card-header">
								<h5>Permisos en productos</h5>
							</div>
							<div class="card-body">
								@foreach ( \App\UserRoleMotorcyclePart::CASTS_DESCRIPTION as $key => $desc)
									
									<div class="form-group row">
										<div class="form-check noselect">
											<input 
												class="form-check-input" 
												type="checkbox" 
												name="role_motorcycle_part__{{$key}}" 
												id="role_motorcycle_part__{{$key}}" 
												value="1" 
												{{ ( isset($record) && $record->hasRole("motorcycle_part__$key") ) ? 'checked' : '' }}>
											<label 
												class="form-check-label" 
												for="role_motorcycle_part__{{$key}}">
												{{ $desc }}
											</label>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="card m-2">
							<div class="card-header">
								<h5>Permisos en adquisiciones</h5>
							</div>
							<div class="card-body">
								@foreach ( \App\UserRoleMotorcyclePartsOrder::CASTS_DESCRIPTION as $key => $desc)
									
									<div class="form-group row">
										<div class="form-check noselect">
											<input 
												class="form-check-input" 
												type="checkbox" 
												name="role_motorcycle_parts_order__{{$key}}" 
												id="role_motorcycle_parts_order__{{$key}}" 
												value="1" 
												{{ ( isset($record) && $record->hasRole("motorcycle_parts_order__$key") ) ? 'checked' : '' }}>
											<label 
												class="form-check-label" 
												for="role_motorcycle_parts_order__{{$key}}">
												{{ $desc }}
											</label>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="card m-2">
							<div class="card-header">
								<h5>Permisos en items de adquisiciones</h5>
							</div>
							<div class="card-body">
								@foreach ( \App\UserRoleMotorcyclePartsOrderItem::CASTS_DESCRIPTION as $key => $desc)
									
									<div class="form-group row">
										<div class="form-check noselect">
											<input 
												class="form-check-input" 
												type="checkbox" 
												name="role_motorcycle_parts_order_item__{{$key}}" 
												id="role_motorcycle_parts_order_item__{{$key}}" 
												value="1" 
												{{ ( isset($record) && $record->hasRole("motorcycle_parts_order_item__$key") ) ? 'checked' : '' }}>
											<label 
												class="form-check-label" 
												for="role_motorcycle_parts_order_item__{{$key}}">
												{{ $desc }}
											</label>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="card m-2">
							<div class="card-header">
								<h5>Permisos en proveedores</h5>
							</div>
							<div class="card-body">
								@foreach ( \App\UserRoleMotorcyclePartsSupplier::CASTS_DESCRIPTION as $key => $desc)
									
									<div class="form-group row">
										<div class="form-check noselect">
											<input 
												class="form-check-input" 
												type="checkbox" 
												name="role_motorcycle_parts_supplier__{{$key}}" 
												id="role_motorcycle_parts_supplier__{{$key}}" 
												value="1" 
												{{ ( isset($record) && $record->hasRole("motorcycle_parts_supplier__$key") ) ? 'checked' : '' }}>
											<label 
												class="form-check-label" 
												for="role_motorcycle_parts_supplier__{{$key}}">
												{{ $desc }}
											</label>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="card m-2">
							<div class="card-header">
								<h5>Permisos en marcas de repuestos</h5>
							</div>
							<div class="card-body">
								@foreach ( \App\UserRoleMotorcyclePartsBrand::CASTS_DESCRIPTION as $key => $desc)
									
									<div class="form-group row">
										<div class="form-check noselect">
											<input 
												class="form-check-input" 
												type="checkbox" 
												name="role_motorcycle_parts_brand__{{$key}}" 
												id="role_motorcycle_parts_brand__{{$key}}" 
												value="1" 
												{{ ( isset($record) && $record->hasRole("motorcycle_parts_brand__$key") ) ? 'checked' : '' }}>
											<label 
												class="form-check-label" 
												for="role_motorcycle_parts_brand__{{$key}}">
												{{ $desc }}
											</label>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="card m-2">
							<div class="card-header">
								<h5>Permisos en personal técnico</h5>
							</div>
							<div class="card-body">
								@foreach ( \App\UserRoleTechnicalStaff::CASTS_DESCRIPTION as $key => $desc)
									
									<div class="form-group row">
										<div class="form-check noselect">
											<input 
												class="form-check-input" 
												type="checkbox" 
												name="role_technical_staff__{{$key}}" 
												id="role_technical_staff__{{$key}}" 
												value="1" 
												{{ ( isset($record) && $record->hasRole("technical_staff__$key") ) ? 'checked' : '' }}>
											<label 
												class="form-check-label" 
												for="role_technical_staff__{{$key}}">
												{{ $desc }}
											</label>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>