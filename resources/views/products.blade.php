@extends('layouts.app')

@section('content')
@include('products.modal_excel_export')
<div class="card">
	<div class="card-header">
		<label>Productos</label>
		<button 
			type="button" 
			class="btn btn-success btn-sm" 
			data-toggle="modal" 
			data-target="#modal_excel_export">
			<i class="fa fa-file-excel-o"></i>
		</button>
		@if(app()->environment('local'))
		<div>
			<form 
				enctype="multipart/form-data" 
				method="post" 
				action="{{ route('motorcycle_parts_import_from_csv') }}">
				@csrf
				<input type="file" name="csv_file">
				<button type="submit" class="btn btn-sm btn-primary">Subir</button>
			</form>
		</div>
		@endif
	</div>

	<div class="card-body">
		<form id="motorcycle_parts_form" method="GET" action="">
			<input type="hidden" name="order_by" value="{{ $order_by ?? '' }}">
			<input type="hidden" name="order" value="{{ $order ?? '' }}">
			<div class="form-check noselect">
				<input 
					class="form-check-input" 
					type="checkbox" 
					value="1" 
					name="show_deleted" 
					{{ isset($show_deleted) ? 'checked' : '' }} 
					id="show_deleted" />
				<label class="form-check-label" for="show_deleted">
					Buscar en eliminados
				</label>
			</div>
			<div class="form-check noselect">
				<input 
					class="form-check-input" 
					type="checkbox" 
					value="1" 
					name="show_who_have_acquisitions" 
					{{ isset($show_who_have_acquisitions) ? 'checked' : '' }} 
					id="show_who_have_acquisitions" />
				<label class="form-check-label" for="show_who_have_acquisitions">
					Buscar productos vinculados a pedidos
				</label>
			</div>
			<br />
			<div class="btn-group mb-1">
				<button 
					type="button" 
					class="btn btn-primary dropdown-toggle btn-sm" 
					data-toggle="dropdown" 
					aria-haspopup="true" 
					aria-expanded="false">
					Paginación de {{ $rowsPerPage }}
				</button>
				<div class="dropdown-menu motorcycle_parts_rows_x_page">
					@foreach([20,50,100,150,200] as $rows_x_page)
					<a class="dropdown-item" rows_x_page="{{ $rows_x_page }}">{{ $rows_x_page }} por página</a>
					@endforeach
				</div>
			</div>
			<input type="hidden" name="page" value="1">
			<input type="hidden" name="rows_x_page" value="{{$rowsPerPage}}">
			<nav aria-label="Paginación de búsqueda" class="nav_pagination">
				<ul class="pagination motorcycle_parts_pagination">
					<li class="page-item {{ $page == 1 ? 'disabled' : '' }}" page="{{ $page - 1 }}">
						<a 
							class="page-link" 
							tabindex="-1" 
							href="{{ $page == 1 ? '' : '#' }}"
							aria-disabled="{{ $page == 1 ? 'true' : 'false' }}">&laquo;</a>
					</li>
					@for($i = 1; $i <= $totalPages; $i++)
						@if($i != $page)
						<li class="page-item" page="{{ $i }}">
							<a class="page-link">{{$i}}</a>
						</li>
						@else
						<li 
							class="page-item active"
							aria-current="page">
							<a class="page-link">
								{{$i}}
								<span class="sr-only">(current)</span>
							</a>
						</li>
						@endif
					@endfor
					<li page="{{ $page + 1 }}"
						class="
							page-item 
							{{ $page == $totalPages ? 'disabled' : '' }} 
						" 
					>
						<a 
							class="page-link" 
							aria-disabled="{{ $page == $totalPages ? 'true' : 'false' }}" 
							href="{{ $page == $totalPages ? '' : '#' }}"
						>&raquo;</a>
					</li>
				</ul>
			</nav><label>Total: {{ $totalRows }}</label>
			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<span class="input-group-text" id="basic-prod-search">
						<i class="fa fa-search"></i>
					</span>
				</div>
				<input 
					name="query"
					type="text" 
					class="form-control" 
					name="query"
					value="{{ $query ?? ''}}"
					placeholder="Búsqueda" 
					aria-label="Búsqueda de productos" 
					aria-describedby="basic-prod-search" />
					<div class="input-group-append">
						<button class="btn btn-success" id="basic-prod-search">
							Buscar
						</button>
					</div>
			</div>
		</form>
		<div class="table-container2">
			<table 
				class="
					table 
					table-hover 
					table-borderer 
					table-stripped">
				<thead class="noselect">
					<tr>
						<th>
							<button 
								class="btn btn-success btn-sm" 
								id="reload-products-table">
								<i class="fa fa-refresh"></i>
							</button>
						</th>
						<th 
							form_tag_id="#motorcycle_parts_form" 
							order_by="prod_name" 
							order="{{ $order_by == 'prod_name' ? $order : '' }}">Producto</th>
						<th 
							form_tag_id="#motorcycle_parts_form" 
							order_by="prod_barcode" 
							order="{{ $order_by == 'prod_barcode' ? $order : '' }}">Código de Barra</th>
						<th 
							form_tag_id="#motorcycle_parts_form" 
							order_by="prod_price" 
							order="{{ $order_by == 'prod_price' ? $order : '' }}">Precio de venta</th>
						<th>Precio de compra</th>
						<th 
							form_tag_id="#motorcycle_parts_form" 
							order_by="prod_stock" 
							order="{{ $order_by == 'prod_stock' ? $order : '' }}">Stock</th>
						<th>Acción</th>
					</tr>
				</thead>
				<tbody>
					@foreach($records as $idx => $record)
						<tr>
							<td>{{ $rowsPerPage * ($page - 1) + $idx + 1 }}</td>
							<td>
								{{ $record->prod_name }}
								@if($show_who_have_acquisitions)
								<{{ $record->total_orders_items_by_barcode() }}>
								<ul>
									@foreach( $record->orders_items_by_barcode AS $order_item )
									@if( ($order = $order_item->order) && ($supplier = $order->supplier))
									<li>
										{{ $supplier->sup_name }}
										<div>
											Código de Pedido / Cotización: {{ $order->order_code }}
										</div>
										<div>
											Comprobante / Factura: {{ $order->order_invoice ?? '' }}
										</div>
									</li>
									@endif
									@endforeach
								</ul>
								@endif
							</td>
							<td>{{ $record->prod_barcode }}</td>
							<td>{{ $record->prod_price }}</td>
							<td>
							<div>{{ round($record->avg_price_from_orders_items_discounting_stock(),2) }}</div>
							<div>
								@php
									$order_items = $record->avg_price_from_orders_items_discounting_stock(1)
								@endphp
								@if(count($order_items)>0)
									<ul>
									@foreach ($order_items as &$order_item)
									@continue( !isset($order_item->order) )
									<li>
										<div>Factura: <b>{{ $order_item->order->order_invoice }}</b></div>
										{{ $order_item->order->bought_at->format('d/m/Y') }}: 
										{{ $order_item->prod_unit_price_with_discount }} 
									</li>
									@endforeach
									</ul>
								@endif
							</div>
							</td>
							<td>
								{{ $record->prod_stock }}
								{{ $record->prod_stock_no_unit > 0 ? " + " . ( $record->prod_stock_no_unit / 1000.0 ) : '' }}</td>
							<td>
								@if( empty($record->deleted_at) )
								<a href="{{ route('motorcycle_parts_show',['id' => $record->id]) }}">
									<button class="btn btn-primary btn-sm">
										Ver
									</button>
								</a>
								@endif
								<button 
									class="btn {{ isset($show_deleted) ? 'btn-warning' : 'btn-danger' }} btn-sm" 
									mod-name="product" 
									del-action 
									record-id="{{ $record->id }}"  
									record-name="{{ $record->prod_name }}">
									{{ isset($show_deleted) ? 'Deshacer eliminación' : 'Eliminar' }}
								</button>
								<form id="form_del_{{ $record->id }}" method="POST" action="{{ route('motorcycle_parts_delete',['id' => $record->id]) }}">
									@csrf
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
