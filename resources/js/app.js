/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')
window.Swal = require('sweetalert2')

window.Vue = require('vue')


import { Datetime } from 'vue-datetime'
// You need a specific loader for CSS files
import 'vue-datetime/dist/vue-datetime.css'
 
Vue.use(Datetime)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('datetime',Datetime)
Vue.component('example-component', require('./components/ExampleComponent.vue').default)
Vue.component('manage-motorcycle-parts-orders', require('./components/ManageMotorcyclePartsOrders.vue').default)
Vue.component('manage-product-items-on-sale', require('./components/ManageProductItemsOnSale.vue').default)
Vue.component('manage-motorcycle-parts-supplier-item-on-sales', require('./components/ManageMotorcyclePartsSupplierItemOnSales.vue').default)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
	el: '#app',
});
require('./sidebar-nav')
require('./alerts')
require('./users')
require('./sales')
require('./products')
require('./technical_staff')

window.axiosErrorToString = function(obj){
	let str = `
	<h4>Algunos de los datos son inválidos</h4>	
	`
	if( obj.errors ){
		for(const idx in obj.errors){
			const arr = obj.errors[idx]
			str += `<ul>`
			for(const msg of arr){
				str += `<li>${msg}</li>`
			}
			str += `</ul>`
		}
	}
	return str
}

jQuery('button[refresh-action]').click(function(evt){
	const obj = jQuery(this)
	const refresh_for = obj.attr('refresh-for')
	jQuery(refresh_for).submit()
})

jQuery('#main-menu').click(function(evt){
	jQuery('#left-side').toggleClass('menu-open')
	jQuery('#right-side').toggleClass('menu-open')
})


jQuery('th[order_by]').click(function(evt){
	const obj = jQuery(this)
	const form_tag_id = obj.attr('form_tag_id')
	let order = ''
	let order_by = ''
	if(obj.attr('order')==''){
		order = 'desc'
	}else if(obj.attr('order')=='asc'){
		order = 'desc'
	}else{
		order = 'asc'
	}
	obj.attr('order',order)
	const form = jQuery(form_tag_id)
	form.find('[name=order_by]').attr('value',obj.attr('order_by'))
	form.find('[name=order]').attr('value',order)
	form.submit()
})