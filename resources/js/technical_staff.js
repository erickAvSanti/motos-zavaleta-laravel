jQuery(window).ready(function(){
	jQuery("button[del-action]").click(function(evt){
		const obj = jQuery(this)
		const id = obj.attr('record-id')
		const name = obj.attr('record-name')
		let mod_name = obj.attr('mod-name')
		if(mod_name == 'technical_staff'){
			mod_name = 'el personal técnico'
		}else{
			return
		}
		const already_deleted = obj.hasClass('btn-warning')
		Swal.fire({
		  title: `Seguro de ${already_deleted ? 'deshacer la eliminación de' : 'eliminar'} ${mod_name} '${name}'?`,
		  text: "",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Si',
		  cancelButtonText: 'Cancelar acción'
		}).then((result) => {
		  if (result.value) {
			jQuery(`#form_del_${id}`).submit()
		  }
		})
	})

	jQuery('.btn-technical-staff-update').click(function(evt){
		jQuery('#technical_staff_form_update').submit()
	})
	jQuery('#technical_staff_form_create input').keydown(function(evt){
		var keyCode = event.keyCode ? event.keyCode : ( event.which ? event.which : event.charCode )
		if( keyCode == 13 ) {
			evt.stopPropagation()
			evt.preventDefault()
		}
	})

	jQuery('.technical_staff_pagination li:not(.disabled)').click(function(evt){
		const page = jQuery(this).attr('page')
		if(/\d+/.test(page)){
			jQuery('input[name=page]').val(page)
			jQuery('#technical_staff_form').submit()
		}
	})

	jQuery(".technical_staff_rows_x_page a").click(function(evt){
		jQuery('input[name=rows_x_page]').val(jQuery(this).attr('rows_x_page'))
		jQuery('#technical_staff_form').submit()
	})

	jQuery('#reload-technical-staff-table').click(function(){
		jQuery('#technical_staff_form').submit()
	})

})