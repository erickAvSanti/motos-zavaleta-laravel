jQuery(window).ready(function(){
	jQuery(".sidebar-nav a:not([href])").click(function(evt){
		const obj = jQuery(this)
		const parent = obj.parent()
		parent.toggleClass('show-nested-children')
		parent.find('ul').toggleClass('show')
	})
})