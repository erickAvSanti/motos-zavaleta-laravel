<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

use \App\Helpers\MotorcyclePartsOrderHelper;
use \Carbon\Carbon;

class MotorcyclePartsOrder extends Model
{
	//
	use SoftDeletes;
	protected $fillable = [
		'order_code',
		'order_invoice',
		'order_receipt_type_id',
		'supplier_id',
		'bought_at'
	];
	protected $casts = [
		'bought_at' => 'date',
	];
	protected $appends = [
		'order_items',
		'order_items_deleted',
		'order_supplier',
		'order_receipt_type',
	];
	//implement the attribute
	public function getOrderItemsAttribute()
	{
	 	return $this->items;
	}
	public function getOrderItemsDeletedAttribute()
	{
	 	return $this->items_deleted;
	}
	public function getOrderSupplierAttribute()
	{
	 	return $this->supplier;
	}
	public function getOrderReceiptTypeAttribute()
	{
	 	return $this->receipt_type;
	}
	public function items(){
    return $this->hasMany('App\MotorcyclePartsOrderItem','order_id');
	}
	public function items_deleted(){
    return $this->hasMany('App\MotorcyclePartsOrderItem','order_id')->onlyTrashed();
	}
	public function supplier(){
   	return $this->belongsTo('App\MotorcyclePartsSupplier','supplier_id');
	}
	public function receipt_type(){
   	return $this->belongsTo('App\ReceiptType','order_receipt_type_id');
	}
	public static function gen_excel_report_by(Request $request){
		$result = MotorcyclePartsOrderHelper::excel_report($request);
		return $result;
	}

	public static function create_new(
			$bought_at, 
			$order_code = null,
			$order_invoice = null,
			$supplier_id = null,
			$order_receipt_type_id = null
		){
		$ret = null;
		if(!empty($order_code) && !empty($order_invoice)){
			$exist = false;
			$record = MotorcyclePartsOrder::where('order_code',$order_code)
								->where(function($query){
									$query->whereNull('order_invoice')
												->orWhere('order_invoice','');
								})
								->first();
			if(!$record){
				$record = MotorcyclePartsOrder::where([
					['order_code',$order_code],
					['order_invoice',$order_invoice],
				])->first();
			}else{

			}
			if(!$record){
				$record = new MotorcyclePartsOrder;
				$record->supplier_id = $supplier_id ?? 1;
				$record->order_receipt_type_id = $order_receipt_type_id ?? 1;
				$record->order_code = $order_code;
				$record->order_invoice = $order_invoice;
				$record->bought_at = Carbon::createFromFormat(
															"!d/m/Y",
															$bought_at);
				$record->save();
			}else{
				$exist = true;
			}
			$ret = ['record' => $record, 'exist' => $exist];
		}
		return $ret;
	}
}
