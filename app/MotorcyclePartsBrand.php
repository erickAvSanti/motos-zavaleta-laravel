<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotorcyclePartsBrand extends Model
{
	//
 	use SoftDeletes;
	protected $fillable = [
		'brand_name',
	];

	public function supplier_item_on_sales(){
		return $this->hasMany('\App\MotorcyclePartsSupplierItemOnSale','brand_id');
	}
	public static function &as_options($request){
		$rows = array(['text'=>'Marcas de repuestos', 'value' => null]);
		foreach (MotorcyclePartsBrand::all() as &$record) {
			$rows[] = [
				'text' => $record->brand_name,
				'value' => $record->id,
			];
		}
		return $rows;
	}

}
