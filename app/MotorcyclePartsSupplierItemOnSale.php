<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//Be careful: We strictly use 'On Sale' instead 'For Sale'.
//both have important meaning...
class MotorcyclePartsSupplierItemOnSale extends Model
{
	//
 	use SoftDeletes;
	protected $fillable = [
		'supplier_item_id',
		'brand_id',
		'item_currency',
		'public_price',
		'public_price_discount',
	];

	protected $appends = [
		'supplier_item',
		'brand',
	];
	public static function create_from_request_props(Request $request){

		
		\Log::info($request->all());
		if(empty($request->item_code)){
			throw new \Exception('item_code_empty');
		}

		$record = MotorcyclePartsSupplierItem::where('item_code',$request->item_code)->first();
		if($record){
			throw new \Exception('item_code_exists');
		}

		/*
			'supplier_item_id' => 3,
			'modify_supplier_item' => false,
			'wait' => true,
			'item_code' => '414147000',
			'prod_name' => 'ABRAZADERA',
			'item_unit' => 'UND',
			'item_type' => '3R',
			'public_price' => 0.31,
			'public_price_discount' => '0.00',
			'brand_id' => 1,
			'motorcycle_model_id' => 304,
		*/
		$supplier_item = new MotorcyclePartsSupplierItem;
		$record = new MotorcyclePartsSupplierItemOnSale;

		DB::transaction(function() use($record, $supplier_item, $request){

			try{
				$supplier_item->item_code = $request->item_code;
				$supplier_item->item_unit = $request->item_unit;
				$supplier_item->item_type = $request->item_type;
				$supplier_item->prod_name = $request->prod_name;
				$supplier_item->motorcycle_model_id = $request->motorcycle_model_id;
				$supplier_item->save();
				
		
				$record->public_price = $request->public_price;
				$record->public_price_discount = $request->public_price_discount;
				$record->brand_id = $request->brand_id;
				$record->item_currency = $request->item_currency;
				$record->supplier_item_id = $supplier_item->id;
				$record->save();
			}catch(\Exception $e){
				\Log::info($e->getMessage());
				\Log::info($e->getTraceAsString());
				throw $e;
			}

		});

		return $record;

	}
	public static function update_from_request_props(Request $request, $id){
		
		/*
		  	'id' => 2,
			'supplier_item_id' => 3,
			'modify_supplier_item' => false,
			'wait' => true,
			'item_code' => '414147000',
			'prod_name' => 'ABRAZADERA',
			'item_unit' => 'UND',
			'item_type' => '3R',
			'public_price' => 0.31,
			'public_price_discount' => '0.00',
			'brand_id' => 1,
			'motorcycle_model_id' => 304,
		*/
		\Log::info($request->all());
		$record = MotorcyclePartsSupplierItemOnSale::find($id);
		$supplier_item = $request->supplier_item_id ? MotorcyclePartsSupplierItem::find($request->supplier_item_id) : null;
		DB::transaction(function() use($request ,$record , $id, $supplier_item){
			try{
				if(
					$request->modify_supplier_item && 
					$supplier_item
				){
					$supplier_item->item_code = $request->item_code;
					$supplier_item->item_unit = $request->item_unit;
					$supplier_item->item_type = $request->item_type;
					$supplier_item->prod_name = $request->prod_name;
					$supplier_item->motorcycle_model_id = $request->motorcycle_model_id;
					$supplier_item->save();
				}
	
				$record->public_price = $request->public_price;
				$record->public_price_discount = $request->public_price_discount;
				$record->brand_id = $request->brand_id;
				$record->item_currency = $request->item_currency;
				$record->save();
			}catch(\Exception $e){
				\Log::info($e->getMessage());
				\Log::info($e->getTraceAsString());
				throw $e;
			}

		});
		return $record;
	}

	public function getSupplierItemAttribute(){
		return $this->belongsTo('\App\MotorcyclePartsSupplierItem','supplier_item_id')->first();
	}

	public function getBrandAttribute(){
		return $this->belongsTo('\App\MotorcyclePartsBrand','brand_id')->first();
	}

	/*
	public function brand(){
		return $this->belongsTo('\App\MotorcyclePartsBrand','brand_id');
	}
	public function supplier_item(){
		return $this->belongsTo('\App\MotorcyclePartsSupplierItem','supplier_item_id');
	}
	*/

	public static function create_from_supplier_catalog_price(&$row,$extra){

		\Log::info("\n\nanalizando fila\n");
		\Log::info($row);

		$builder = new MotorcyclePartsSupplierItemOnSale;
		$record = $builder
							->where([
								['supplier_item_id',$extra['supplier_item']->id],
								['brand_id',$extra['brand_id']],
							])
							->first();
		
		if(!$record){

			$record = new MotorcyclePartsSupplierItemOnSale;
			$record->supplier_item_id = $extra['supplier_item']->id;
			$record->brand_id 				= $extra['brand_id'];

		}

		$record->item_currency = trim($row[5]) ?? 'S/';

		$public_price = trim(preg_replace('/,/', '', $row[6]));
		$public_price_discount = trim(preg_replace('/,/', '', $row[7]));

		$record->public_price = is_numeric($public_price) ? $public_price : 0;
		$record->public_price_discount = is_numeric($public_price_discount) ? $public_price_discount : 0;

		$record->save();

		return $record;
		
	}
	public static function currency_options(){
		$arr = MotorcyclePartsSupplierItemOnSale::select('item_currency')
						->distinct()
						->get();
		if(count($arr) == 1)return null;
		$options = [['text' => 'Todos', 'value' => null]];
		foreach ($arr as &$prop) {
			$value = $prop->item_currency;
			if($value == '')$value="Vacío";
			$options[] = ['text' => $value, 'value' => $value];
		}
		return $options;

	}
	public function toCustomArray(){

		$arr = $this->toArray();
		foreach ($arr as $key => &$value) {
			if(is_string($value) && preg_match('/^\d+$/',$value)===1){
				$value = (int)$value;
			}else if(is_numeric($value)){
				$value = (float)$value;
			}
		}
		return $arr;
	}
}
