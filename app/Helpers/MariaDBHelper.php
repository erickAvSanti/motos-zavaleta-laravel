<?php
namespace App\Helpers;
use \Carbon\Carbon;
class MariaDBHelper{
    public static function make_a_full_db_backup_to_directory($directory = null, $extra_args_db = "", $return_only_filename = false){
        if(!$directory){
        
            $directory = storage_path('app/mariadb_helper_backups/');
            if(!is_dir($directory)){
                mkdir($directory,0777);
            }
        }
        $end_char_directory = substr($directory,-1);
        if($end_char_directory !== '/')$directory .= '/';
        $filename = env('DB_DATABASE')."_".Carbon::now()->format('Y_m_d__H_i_s').".sql";
        $file_path = $directory . $filename;
        shell_exec("mysqldump -u".env('DB_USERNAME').( !empty(env('DB_PASSWORD')) ? " -p".env('DB_PASSWORD') : '')." ".env('DB_DATABASE')." $extra_args_db --skip-tz-utc > ".$file_path);
        if(file_exists($file_path)){
            if($return_only_filename){
                return $filename;
            }else{
                return $file_path;
            }
        }
        return null;
    }
}