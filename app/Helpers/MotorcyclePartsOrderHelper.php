<?php 
namespace App\Helpers;
use \App\MotorcyclePartsOrder;
use \App\MotorcycleModel;
use \App\MotorcyclePartsOrderItem;
use \App\MotorcyclePart;
use \App\MotorcyclePartsSupplier;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \Carbon\Carbon;
use \PhpOffice\PhpSpreadsheet\Style\Fill;
use \PhpOffice\PhpSpreadsheet\Style\Alignment;
use \PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Illuminate\Http\Request;
define('STORAGE_MOTORCYCLE_PARTS_ORDERS_REPORT', 
	storage_path() . '/app/public/motorcycle_parts_orders'
);
if(!is_dir(STORAGE_MOTORCYCLE_PARTS_ORDERS_REPORT)){
	mkdir(STORAGE_MOTORCYCLE_PARTS_ORDERS_REPORT,0711,true);
}
class MotorcyclePartsOrderHelper{
	
	public static function excel_report(Request $request = null){
		$rows = [];
		$records = [];
		if(!isset($request) || !$request->by){
			$records = MotorcyclePartsOrder::all();
		}else{
			if(
				$request->by == 'order_code' && 
				!empty($request->value)
			){
				$records = MotorcyclePartsOrder::where('order_code', $request->value)->get();
			}
			if(
				$request->by == 'order_code_invoice' && 
				!empty($request->value) && 
				!empty($request->value1)
			){
				$records = MotorcyclePartsOrder::where([
					['order_code', $request->value],
					['order_invoice', $request->value1],
				])->get();
			}
		}
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('Principal');
		$sheet->getColumnDimension('A')->setWidth(14);
		$sheet->getColumnDimension('B')->setWidth(14);
		$sheet->getColumnDimension('C')->setWidth(14);
		$sheet->getColumnDimension('D')->setWidth(14);
		$sheet->getColumnDimension('E')->setWidth(14);
		$sheet->getColumnDimension('F')->setWidth(12);
		$sheet->getColumnDimension('G')->setWidth(20);
		$sheet->getColumnDimension('H')->setWidth(25);
		$sheet->getColumnDimension('I')->setWidth(12);
		$sheet->getColumnDimension('J')->setWidth(12);
		$sheet->getColumnDimension('K')->setWidth(12);
		$sheet->getColumnDimension('L')->setWidth(12);
		$sheet->getColumnDimension('M')->setWidth(12);
		$sheet->getColumnDimension('N')->setWidth(12);
		$sheet->getColumnDimension('O')->setWidth(12);
		$sheet->getColumnDimension('P')->setWidth(50);
		$sheet->getColumnDimension('Q')->setWidth(10);
		$sheet->getColumnDimension('R')->setWidth(10);
		$sheet->getColumnDimension('S')->setWidth(10);
		$sheet->getColumnDimension('T')->setWidth(10);

		for($i = 'A'; $i <= 'S'; $i++){
			self::setSheetWrapText($sheet,$i,true);
			self::setSheetHorizontalAlignment($sheet,"{$i}1",Alignment::HORIZONTAL_CENTER);
			self::setSheetVerticalAlignment($sheet,"{$i}1",Alignment::VERTICAL_CENTER);
		}

		$rows[] = [
			'Cotización / Pedido',
			'Proveedor',
			'Tipo Documento',
			'Comprobante / Factura',
			'Fecha',
			'Código de Item',
			'Código de Barra',
			'Descripción de producto',
			'Cantidad',
			'Precio unitario de venta',
			'Precio unitario de compra',
			'Precio unitario de compra con descuento',
			'Descuento (%)',
			'Descuento Unitario (S/.)',
			'Descuento Total (S/.)',
			'Total',
			'Modelo'
		];

		foreach ( $records as &$record ) {
			$items = $record->order_items;
			if(count($items)>0){
				foreach ($items as &$item) {
					$rows[] = [
						$record->order_code,
						$record->order_supplier->sup_name ?? '',
						$record->order_receipt_type->rt_name ?? '',
						$record->order_invoice,
						$record->bought_at ? 
							$record->bought_at->format('d/m/Y') : '-',
						$item->order_item_code,
						$item->prod_barcode,
						$item->prod_name,
						$item->order_item_quantity,
						(float)$item->prod_unit_price_on_sale,
						(float)$item->prod_unit_price,
						(float)$item->prod_unit_price * ( 100.0 - $item->order_item_percent_discount ) / 100.0 ,
						(float)$item->order_item_percent_discount / 100.0,
						$item->prod_unit_price * $item->order_item_percent_discount /100.0 ,
						( $item->prod_unit_price * $item->order_item_quantity ) * $item->order_item_percent_discount /100.0 ,
						( $item->prod_unit_price * $item->order_item_quantity ) * ( 100.0 - $item->order_item_percent_discount )/100.0 ,
						( $item->motorcycle_model ? $item->motorcycle_model->mo_name : '-' ),
					];
				}
			}
		}
		$sheet->fromArray($rows,NULL, 'A1');
		for($i = 'A'; $i <= 'Q'; $i++){
			for($j = 1; $j <= count($rows); $j++){
				self::setSheetBorderColor($sheet,"{$i}${j}",'FF000000');
			}
		}

		$decimals_format = ['I','J','K','L','M','O','P'];
		$prefix_row_index = 2;
		foreach ($rows as $row_index => &$row) {
			foreach ($row as $col_index => &$col) {
				$char = chr($col_index + ord('A'));
				if( in_array( $char , $decimals_format ) ){
					if($char == 'M'){
						self::setSheetDecimalFormat($sheet,"$char" . ($row_index + $prefix_row_index), NumberFormat::FORMAT_PERCENTAGE);
						//self::setSheetDecimalFormat($sheet,"$char" . ($row_index + $prefix_row_index) );
					}else if($char == 'I'){
						self::setSheetDecimalFormat($sheet,"$char" . ($row_index + $prefix_row_index), NumberFormat::FORMAT_NUMBER);
					}else{
						self::setSheetDecimalFormat($sheet,"$char" . ($row_index + $prefix_row_index));
					}
				}
			}
		}

		

		$writer = new Xlsx($spreadsheet);
		$filename = STORAGE_MOTORCYCLE_PARTS_ORDERS_REPORT . "/reporte_excel_" . 
			\Carbon\Carbon::now()->setTimezone(env('APP_TIMEZONE'))
			->format('d_m_Y_h_i_s_a') . '_' . \Str::random(20) . ".xlsx";
		
		$writer->save($filename);
		
		return self::toPublicExcelReportLink($filename);
	
	}
	public static function toPublicExcelReportLink($filename)
	{
		return preg_replace('/.+\/(motorcycle_parts_orders.+)$/',"storage/$1",$filename);
	}

	public static function setSheetBold( &$sheet, $cell, $flag = true)
	{
		return $sheet
			->getStyle($cell)
			->getFont()
			->setBold($flag);
	}

	public static function setSheetColor( &$sheet, $cell, $color)
	{
		return $sheet
			->getStyle($cell)
			->getFont()
			->getColor()
			->setARGB($color);
	}

	public static function setSheetFill( &$sheet, $cell, $color)
	{
		return $sheet
			->getStyle($cell)
			->getFill()
			->setFillType(Fill::FILL_SOLID)
			->getStartColor()->setARGB($color);
	}

	public static function setSheetHorizontalAlignment(&$sheet, $cell, $align)
	{

		return $sheet
			->getStyle($cell)
  		->getAlignment()
  		->setHorizontal($align);
	}

	public static function setSheetVerticalAlignment(&$sheet, $cell, $align)
	{

		return $sheet
			->getStyle($cell)
  		->getAlignment()
  		->setVertical($align);
	}

	public static function setSheetWrapText(&$sheet, $cell, $wrap = true)
	{

		return $sheet
			->getStyle($cell)
  		->getAlignment()
  		->setWrapText($wrap);
	}

	public static function setSheetDateFormat(&$sheet, $cell, $format = NumberFormat::FORMAT_DATE_YYYYMMDDSLASH )
	{

		$sheet->getStyle($cell)
    ->getNumberFormat()
    ->setFormatCode($format);
	}

	public static function setSheetDecimalFormat(&$sheet, $cell, $format = NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1 )
	{

		$sheet->getStyle($cell)
    ->getNumberFormat()
    ->setFormatCode($format);
	}

	public static function setSheetBorderColor(&$sheet, $cell, $color)
	{
		$borders = $sheet->getStyle($cell)
    ->getBorders();

		$borders->getTop()->setBorderStyle(
    	\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

		$borders->getTop()->getColor()->setARGB($color);
    
		$borders->getBottom()->setBorderStyle(
    	\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

		$borders->getBottom()->getColor()->setARGB($color);
    
		$borders->getLeft()->setBorderStyle(
    	\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

		$borders->getLeft()->getColor()->setARGB($color);
    
		$borders->getRight()->setBorderStyle(
    	\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

		$borders->getRight()->getColor()->setARGB($color);

	}	

	public static function import_from_csv()//first data importing
	{
		$rows = [];
		$fila = 0;
		$file_path = __DIR__ . "/../../storage/app/public/motos.csv";
			if (($gestor = fopen($file_path, "r")) !== FALSE) {
				while(
					($datos = fgetcsv($gestor, 3000, ",")) !== FALSE
				){
					$fila++;
					if($fila == 1)continue;
					$rows[] = $datos;
				}
				fclose($gestor);
			}
			/*
			
		0 => 'G4060170  ',
		1 => '1',
		2 => '150584',
		3 => '25/06/2020',
		4 => '92',
		5 => 'MOTOR ARRANCADOR COMPLETO',
		6 => '3',
		7 => '3',
		8 => '249.4',
		9 => '30',
		10 => '174.58',
		11 => '523.74',
		12 => 'KING TM / TM',
		*/
		usort($rows,function($a,$b){
			if ($a[1] == $b[1]) {
        return 0;
    	}
    	return ($a[1] < $b[1]) ? -1 : 1;
		});
		//\Log::info($rows);
		$current_number_order = 0;
		$current_order_code = 0;
		$current_order_record = null;
		foreach ($rows as &$row) {

			$order_item_code = trim($row[0]);
			if(empty($order_item_code))continue;

			$order_group 									= trim($row[1]);
			$order_code 									= trim($row[2]);
			$bought_at 										= trim($row[3]);
			$prod_name 										= trim($row[5]);
			$order_item_quantity 					= trim($row[6]);
			$prod_unit_price 							= trim($row[8]);
			$order_item_percent_discount 	= trim($row[9]);
			$motorcycle_model_mo_name 		= trim($row[12]);

			$motorcycle_model = MotorcycleModel::create_from_name($motorcycle_model_mo_name);

			$order_exist = false;
			if( $current_order_code != $order_code || !$current_order_record ){
				$current_order_code = $order_code;
				$ret = MotorcyclePartsOrder::create_new($bought_at,$order_code);
				if($ret){
					$current_order_record = $ret['record'];
					$order_exist	= $ret['exist'];
				}else{
					continue;
				}
			}

			$order_item = MotorcyclePartsOrderItem::where([
				['order_id',$current_order_record->id],
				['order_item_code',$order_item_code],
			])->first();

			\Log::info("\n\n");
			\Log::info("order_group = $order_group\n");
			\Log::info("order_code = $order_code\n");
			\Log::info("bought_at = $bought_at\n");
			\Log::info("prod_name = $prod_name\n");
			\Log::info("order_item_quantity = $order_item_quantity\n");
			\Log::info("prod_unit_price = $prod_unit_price\n");
			\Log::info("order_item_percent_discount = $order_item_percent_discount\n");
			\Log::info("motorcycle_model_mo_name = $motorcycle_model_mo_name\n");



			if(!$order_item){
				$order_item = new MotorcyclePartsOrderItem;
				$order_item->order_item_code = $order_item_code;
				$order_item->prod_barcode = $order_item_code;
				$order_item->order_item_percent_discount = $order_item_percent_discount;
				$order_item->order_item_quantity = $order_item_quantity;
				$order_item->prod_name = utf8_decode($prod_name);
				$order_item->prod_unit_price = $prod_unit_price;
				$order_item->motorcycle_model_id = $motorcycle_model->id;
				$order_item->order_id = $current_order_record->id;
				try {
					$order_item->save();

					$product = MotorcyclePart::where('prod_barcode',$order_item_code)->first();
					if(!$product){
						$product = new MotorcyclePart;
						$product->prod_name = utf8_encode($prod_name);
						$product->prod_barcode = $order_item_code;
						$product->prod_stock = $order_item_quantity;
						$product->prod_price =  $prod_unit_price * ( 100.0 - $order_item_percent_discount ) / 100.0;
						
					}else{
						if(!$order_exist){
							$product->prod_stock += $order_item_quantity;
						}
					}
					$product->save();

				} catch (\Exception $e) {
					\Log::info($e->getMessage());
					return;
				}
			}
		}
  	}


	public static function import_from_csv2_to_update_product_prices()
	{
		$rows = [];
		$fila = 0;
		$file_path = __DIR__ . "/../../storage/app/public/motos3.csv";
		if (($gestor = fopen($file_path, "r")) !== FALSE) {
		 	while(
		 		($datos = fgetcsv($gestor, 3000, ",")) !== FALSE
		 	){
				$fila++;
				if($fila == 1)continue;
				$rows[] = $datos;
			}
			fclose($gestor);
		}
		foreach ($rows as $key_row => &$row) {

			foreach($row AS $kk => &$vv){
				if(is_string($vv)){
					$row[$kk] = trim(preg_replace('/\s+/',' ',$vv));
					if(is_numeric($vv)){
						$row[$kk] = preg_replace('/,/','',$row[$kk]);
					}
				}
			}

			$order_item_code 				= $row[0];
			$prod_unit_price  				= $row[10];
			$product = MotorcyclePart::where('prod_barcode',$order_item_code)->first();
			if($product){
				$product->prod_price = $prod_unit_price;
				$product->save();
			}
		}
	}

	public static function import_from_csv2()//first data importing
	{
		$rows = [];
		$fila = 0;
		//$file_path = __DIR__ . "/../../storage/app/public/motos3.csv";
		$file_path = storage_path("app/public/motos3.csv");
		if(!\is_file($file_path)){
			\Log::info("file not found: $file_path");
		}
		if (($gestor = fopen($file_path, "r")) !== FALSE) {
		 	while(
		 		($datos = fgetcsv($gestor, 3000, ",")) !== FALSE
		 	){
				$fila++;
				if($fila == 1)continue;
				$rows[] = $datos;
			}
			fclose($gestor);
		}
		usort($rows,function($a,$b){
			if ($a[4] == $b[4]) {
        return 0;
    	}
    	return ($a[4] < $b[4]) ? -1 : 1;
		});
		//\Log::info($rows);
		$current_number_order = 0;
		$current_order_code = 0;
		$current_order_invoice = 0;
		$current_order_record = null;
		foreach ($rows as $key_row => &$row) {

			foreach($row AS $kk => &$vv){
				if(is_string($vv)){
					$row[$kk] = trim(preg_replace('/\s+/',' ',$vv));
					if(is_numeric($vv)){
						$row[$kk] = preg_replace('/,/','',$row[$kk]);
					}
				}
			}

			$order_item_code 				= $row[0];
			$business_name 					= $row[1];
			$document_type 					= $row[2];
			$order_invoice 					= $row[3];
			$order_group 					= $row[4];
			$order_code 					= $row[5];
			$bought_at 						= $row[6];
			$prod_name 						= $row[8];
			$order_item_quantity 			= $row[9];
			$prod_unit_price  				= $row[10];
			$order_item_percent_discount 	= $row[11];
			$motorcycle_model_mo_name 		= $row[14];

			if(!is_numeric($order_item_percent_discount)){
				$order_item_percent_discount = 30;
			}

			\Log::info("\n\n");
			\Log::info("index = $key_row");
			\Log::info($row);


			if(empty($order_item_code)){
				\Log::info('Empty item code');
				continue;
			}
			if( preg_match('/^\w+$/',$order_item_code) !==1 ){
				\Log::info('item code with bad values ' . $order_item_code);
				continue;
			}

			if(empty($motorcycle_model_mo_name)){
				\Log::info('motorcycle_model_mo_name empty');
				continue;
			}

			if(preg_match('/modelo/i', $motorcycle_model_mo_name) === 1){
				\Log::info('motorcycle_model_mo_name invalid');
				continue;
			}

			if(empty($prod_name)){
				\Log::info('prod_name empty');
				continue;
			}

			if(empty($business_name)){
				\Log::info('business_name empty');
				continue;
			}

			if( preg_match('/^\d{1,2}\/\d{1,2}\/\d{4}$/',$bought_at) !== 1 ){
				\Log::info('bought_at bad format');
				continue;
			}

			if( preg_match('/^[\w\-]+$/',$order_invoice) !== 1 ){
				\Log::info('order_invoice bad format');
				continue;
			}

			if( preg_match('/^\d+$/',$order_code) !== 1 ){
				\Log::info('order_code bad format');
				continue;
			}

			if( !is_numeric($order_item_quantity) ){
				\Log::info('order_item_quantity is not numeric');
				continue;
			}

			if( !is_numeric($prod_unit_price) ){
				\Log::info('prod_unit_price is not numeric');
				continue;
			}

			$motorcycle_model = MotorcycleModel::create_from_name($motorcycle_model_mo_name);
			$supplier = MotorcyclePartsSupplier::create_from_name($business_name);

			$order_exist = false;
			if( 
					$current_order_code != $order_code || 
					$current_order_invoice != $order_invoice || 
					!$current_order_record 
			){
				$current_order_code = $order_code;
				$current_order_invoice = $order_invoice;
				$ret = MotorcyclePartsOrder::create_new(
								$bought_at,
								$order_code, 
								$order_invoice,
								$supplier->id
							);
				if($ret){
					$current_order_record = $ret['record'];
					$order_exist	= $ret['exist'];
				}else{
					\Log::info('skipping ' . $bought_at . ' ' . $order_code . ' ' . $order_invoice);
					continue;
				}
			}
			\Log::info($current_order_record->id);
			\Log::info("order_exist => " . ($order_exist ? 'si' : 'no'));

			$order_item = MotorcyclePartsOrderItem::where([
				['order_id',$current_order_record->id],
				['order_item_code',$order_item_code],
			])->first();

			if(!$order_item){
				$order_item = new MotorcyclePartsOrderItem;
				$order_item->order_item_code = $order_item_code;
				$order_item->prod_barcode = $order_item_code;
				$order_item->order_item_percent_discount = $order_item_percent_discount;
				$order_item->order_item_quantity = $order_item_quantity;
				$order_item->prod_name = utf8_decode($prod_name);
				$order_item->prod_unit_price = $prod_unit_price;
				$order_item->motorcycle_model_id = $motorcycle_model->id;
				$order_item->order_id = $current_order_record->id;
				try {
					$order_item->save();

					$product = MotorcyclePart::where('prod_barcode',$order_item_code)->first();
					if(!$product){
						$product = new MotorcyclePart;
						$product->prod_name = utf8_encode($prod_name);
						$product->prod_barcode = $order_item_code;
						$product->prod_stock = $order_item_quantity;
						//$product->prod_price =  $prod_unit_price * ( 100.0 - $order_item_percent_discount ) / 100.0;
						$product->prod_price =  $prod_unit_price;
						
					}else{
						if(!$order_exist){
							$product->prod_stock += $order_item_quantity;
						}else{
							if(
								$product->prod_stock > 0 && 
								$product->prod_stock != $order_item_quantity
							){
								$product->prod_stock = $order_item_quantity;
							}
						}
					}
					$product->save();

				} catch (\Exception $e) {
					\Log::info($e->getMessage());
					return;
				}
			}
		}
  	}
}