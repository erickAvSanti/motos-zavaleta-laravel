<?php
namespace App\Helpers;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \Carbon\Carbon;
use \App\MotorcyclePart;
use \App\MotorcyclePartsSupplierItem;
use \App\MotorcyclePartsSupplierItemOnSale;
use \App\MotorcycleModel;
use \PhpOffice\PhpSpreadsheet\Style\Fill;
use \PhpOffice\PhpSpreadsheet\Style\Alignment;
class ImportExcelHelper{

	public static function mix_worksheets_supplier_catalog()
	{
		$inputFileNames = [
			[
			'filename' => __DIR__ . '/../../storage/app/public/Lista de Precios BAJAJ julio 2020.xlsx',
			'supplier_id' => 2,
			],
			[
			'filename' => __DIR__ . '/../../storage/app/public/Lista de Precios TVS Julio 2020.xlsx',
			'supplier_id' => 1,
			],
		];
		foreach ($inputFileNames as &$inputFile) {
			$real_rows = array();
			$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFile['filename']);
			foreach ($spreadsheet->getAllSheets() as $index => &$sheet) {
				echo "\n" . $sheet->getTitle();
				$worksheet = $spreadsheet->setActiveSheetIndex($index);
				$rows = $worksheet->toArray();
				foreach ($rows as $key_row => &$row) {
					$continue = false;
					foreach ($row as $key => &$value) {
						if(
							preg_match("/modelo/i", $value) === 1 || 
							preg_match("/tipo/i", $value) === 1 || 
							preg_match("/precio/i", $value) === 1 || 
							preg_match("/ordenado/i", $value) === 1 
						){
							$continue = true;
							break;
						}
					}
					if($continue)continue;
					if(preg_match('/[a-zA-Z0-9]+/', $row[0]) === 1){
						if(empty($row[2])){
							array_splice($row, 2, 1);
						}else{
							if(
								preg_match("/und/i",$row[2]) !== 1 && 
								preg_match("/kit/i",$row[2]) !== 1 
							){
								$row[1] = $row[1] . " " . $row[2];
								array_splice($row, 2, 1);
							}
						}
						if(trim($row[2]) == '2R' || trim($row[2]) == '3R'){
							array_splice($row, 2, 0, '');
						}
						if(!empty($row[5]) && trim($row[5]) != 'S/' && empty($row[4])){
							$row[4] = $row[5];
							array_splice($row, 5, 1);
						}
						if( isset($row[7]) && is_numeric($row[7]) && $row[6] == 'S/' && empty($row[5])){
							$row[5] = $row[6];
							$row[6] = $row[7];
							array_splice($row, 7, 1);
						}
						if( 
							isset($row[7]) && 
							($num = preg_replace('/,/','',$row[7])) && 
							is_numeric($num) && 
							$row[6] == 'S/' && 
							empty($row[5]) 
						){
							$row[5] = $row[6];
							$row[6] = $row[7];
							array_splice($row, 7, 1);
						}
						$real_rows[] = $row;
					}
				}
			}
			self::save_worksheets_mixed_from_supplier_catalog($real_rows,$inputFile);
		}
	}
	public static function save_worksheets_mixed_from_supplier_catalog(&$rows, &$file_info){

		$filename = $file_info['filename'];
		$filename = preg_replace('/\/([\w\s]+)\.xlsx$/', '/\\1 mixed.xlsx',$filename);

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('Principal');
		$sheet->fromArray($rows,NULL, 'A1');

		$writer = new Xlsx($spreadsheet);
		$writer->save($filename);
	}

	public static function import_from_supplier_catalog()
	{
		$inputFileNames = [
			[
			'filename' => __DIR__ . '/../../storage/app/public/Lista de Precios BAJAJ julio 2020 mixed.xlsx',
			'supplier_id' => 2,
			],
			[
			'filename' => __DIR__ . '/../../storage/app/public/Lista de Precios TVS Julio 2020 mixed.xlsx',
			'supplier_id' => 1,
			],
		];
    	foreach ($inputFileNames as &$inputFile) {
      		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFile['filename']);
      		foreach ($spreadsheet->getAllSheets() as $index => &$sheet) {
				echo "\n" . $sheet->getTitle();
				$worksheet = $spreadsheet->setActiveSheetIndex($index);
				$rows = $worksheet->toArray();
				self::process_supplier_catalog_row($rows,$inputFile['supplier_id']);
      		}
    	}
	}
  	public static function process_supplier_catalog_row(&$rows, $supplier_id){
    	foreach ( $rows as &$row ) {
			if( preg_match('/\w+/', $row[0])===1 ){
				if( array_key_exists(7, $row) && !empty($row[7]) ){
				$motorcycle_model = MotorcycleModel::create_from_name($row[5]);
				}else{
				$motorcycle_model = MotorcycleModel::create_from_name($row[4]);
				}
				MotorcyclePartsSupplierItem::create_from_supplier_catalog($row,[
				'motorcycle_model' => $motorcycle_model,
				'supplier_id' => $supplier_id,
				]);
			}
    	}
  	}

	public static function import_from_supplier_catalog_prices_v1()
	{
		$inputFileName = __DIR__ . '/../../storage/app/public/CATALOGO DE PRECIOS TVS - BAJAJ JUL 2020.xlsx';
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
		
		//TVS
		$worksheet = $spreadsheet->setActiveSheetIndex(0);
		$rows = $worksheet->toArray();
		self::process_supplier_catalog_prices_row($rows,1);
		
		//BAJAJ
		$worksheet = $spreadsheet->setActiveSheetIndex(1);
		$rows = $worksheet->toArray();
		self::process_supplier_catalog_prices_row($rows,2);
	}

	public static function process_supplier_catalog_prices_row(&$rows,$brand_id){
		foreach ($rows as &$row) {
			if(empty($row[0]) || preg_match("/codigo|precio/i",$row[0]) === 1){
				continue;
			}
   			$motorcycle_model = MotorcycleModel::create_from_name($row[4]);
   			$supplier_item = 	MotorcyclePartsSupplierItem::create_from_supplier_catalog_price(
									$row,
									[
										'motorcycle_model' => $motorcycle_model,
										'brand_id' => $brand_id,
									]
								);
   			$supplier_item_on_sale = 	MotorcyclePartsSupplierItemOnSale::create_from_supplier_catalog_price(
											$row,
											[
												'motorcycle_model' => $motorcycle_model,
												'supplier_item' => $supplier_item,
												'brand_id' => $brand_id,
											]
										);
		}
	}


}