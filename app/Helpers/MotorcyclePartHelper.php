<?php
namespace App\Helpers;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \Carbon\Carbon;
use \App\MotorcyclePart;
use \PhpOffice\PhpSpreadsheet\Style\Fill;
use \PhpOffice\PhpSpreadsheet\Style\Alignment;
class MotorcyclePartHelper{

	public static function excel_export_all($to_url = false)
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$rows[] = [
			'Código de barra',
			'Producto',
			'Precio de venta por unidad/kilo/litro',
			'Stock',
			'Eliminado ?',
		];
		foreach (MotorcyclePart::withTrashed()->get() as $kk => &$record) {
			$rows[] = [
				$record->prod_barcode,
				$record->prod_name,
				$record->prod_price,
				$record->prod_stock,
				( $record->deleted_at ? 'Eliminado' : '' ),
			];
		}

		$sheet->getColumnDimension('A')->setWidth(20);
		$sheet->getColumnDimension('B')->setWidth(35);
		$sheet->getColumnDimension('C')->setWidth(19);
		$sheet->getColumnDimension('E')->setWidth(19);
		$sheet->getColumnDimension('F')->setWidth(30);

		self::setSheetWrapText($sheet,'C1');

		self::setSheetVerticalAlignment($sheet,'A1',Alignment::VERTICAL_CENTER);
		self::setSheetVerticalAlignment($sheet,'B1',Alignment::VERTICAL_CENTER);
		self::setSheetVerticalAlignment($sheet,'C1',Alignment::VERTICAL_CENTER);
		self::setSheetVerticalAlignment($sheet,'D1',Alignment::VERTICAL_CENTER);
		self::setSheetVerticalAlignment($sheet,'E1',Alignment::VERTICAL_CENTER);
		self::setSheetVerticalAlignment($sheet,'F1',Alignment::VERTICAL_CENTER);

		self::setSheetHorizontalAlignment($sheet,'A1',Alignment::HORIZONTAL_CENTER);
		self::setSheetHorizontalAlignment($sheet,'B1',Alignment::HORIZONTAL_CENTER);
		self::setSheetHorizontalAlignment($sheet,'C1',Alignment::HORIZONTAL_CENTER);
		self::setSheetHorizontalAlignment($sheet,'D1',Alignment::HORIZONTAL_CENTER);
		self::setSheetHorizontalAlignment($sheet,'E1',Alignment::HORIZONTAL_CENTER);
		self::setSheetHorizontalAlignment($sheet,'F1',Alignment::HORIZONTAL_CENTER);
		$sheet->getRowDimension('1')->setRowHeight(30);

		$sheet->fromArray($rows,NULL, 'A1');
		$writer = new Xlsx($spreadsheet);
		$filename = STORAGE_PRODUCT . "/full_" . \Carbon\Carbon::now()->setTimezone(env('APP_TIMEZONE'))->format('d_m_Y_h_i_s_a') . '_' . \Str::random(20) . ".xlsx";
		$writer->save($filename);
		if($to_url){
			return self::toPublicExcelReportLink($filename);
		}else{
			return $filename;
		}
	}

	public static function toPublicExcelReportLink($filename)
	{
		return preg_replace('/.+\/(products.+)$/',"storage/$1",$filename);
	}

	public static function setSheetBold( &$sheet, $cell, $flag = true)
	{
		return $sheet
			->getStyle($cell)
			->getFont()
			->setBold($flag);
	}

	public static function setSheetColor( &$sheet, $cell, $color)
	{
		return $sheet
			->getStyle($cell)
			->getFont()
			->getColor()
			->setARGB($color);
	}

	public static function setSheetFill( &$sheet, $cell, $color)
	{
		return $sheet
			->getStyle($cell)
			->getFill()
			->setFillType(Fill::FILL_SOLID)
			->getStartColor()->setARGB($color);
	}

	public static function setSheetHorizontalAlignment(&$sheet, $cell, $align)
	{

		return $sheet
			->getStyle($cell)
  		->getAlignment()
  		->setHorizontal($align);
	}

	public static function setSheetVerticalAlignment(&$sheet, $cell, $align)
	{

		return $sheet
			->getStyle($cell)
  		->getAlignment()
  		->setVertical($align);
	}

	public static function setSheetWrapText(&$sheet, $cell, $wrap = true)
	{

		return $sheet
			->getStyle($cell)
  		->getAlignment()
  		->setWrapText($wrap);
	}

}