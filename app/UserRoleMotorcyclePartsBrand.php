<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRoleMotorcyclePartsBrand extends Model
{
	//
	protected $fillable = [
		'user_id',
		'list_records',
		'view_record',
		'update_record',
		'create_record',
		'delete_record',
		'export_excel',
	];
	protected $casts = [
		'list_records' => 'boolean',
		'view_record' => 'boolean',
		'update_record' => 'boolean',
		'create_record' => 'boolean',
		'delete_record' => 'boolean',
		'export_excel' => 'boolean',
	];
	const CASTS_DESCRIPTION = [
		'list_records' => 'Listar marcas de repuestos',
		'view_record' => 'Ver marca de repuesto',
		'update_record' => 'Actualizar marca de repuesto',
		'create_record' => 'Crear marca de repuesto',
		'delete_record' => 'Eliminar marca de repuesto',
	];
	public function getCasts(){
		return $this->casts;
	}
	public function user()
  {
   	return $this->belongsTo('App\User','user_id');
  }
  public function resetProps(){
  	foreach (self::CASTS_DESCRIPTION as $key => &$value) {
  		$this[$key] = false;
  	}
  	$this->save();
  }
}
