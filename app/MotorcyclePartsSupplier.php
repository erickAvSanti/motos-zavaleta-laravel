<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotorcyclePartsSupplier extends Model
{
	//
	use SoftDeletes;
	protected $fillable = [
		'sup_name',
		'sup_document',
		'sup_document_type',
	];
	public function orders(){
    	return $this->hasMany('App\MotorcyclePartsOrder','supplier_id');
	}

	public static function create_from_name($business_name){
		
		$record = MotorcyclePartsSupplier::where('sup_name',$business_name)->first();
		if(!$record){
			$faker = \Faker\Factory::create();
			$record = new MotorcyclePartsSupplier;
			$record->sup_name = $business_name;
			$record->sup_document = "20".$faker->randomNumber(9);
			$record->sup_document_type = 'RUC';
			$record->save();
		}
		return $record;

	}

}
