<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class TechnicalStaff extends Model
{
	//
	use SoftDeletes;
	protected $fillable = [
		'staff_name',
	];
  public function sales()
  {
    return $this->hasMany('App\Sale','technical_staff_id');
  }
  public function sales_histories()
  {
    return $this->hasMany('App\SaleHistory','technical_staff_id');
  }
	public static function as_options(Request $request){
		$rows = array(['text'=>'Seleccionar técnico', 'value' => null]);
		foreach (TechnicalStaff::all() as &$record) {
			$rows[] = [
				'text' => $record->staff_name,
				'value' => $record->id,
			];
		}
		return response()->json($rows);
	}
}
