<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

define('STORAGE_PRODUCT', storage_path() . '/app/public/products');
if(!is_dir(STORAGE_PRODUCT)){
	mkdir(STORAGE_PRODUCT,0777,true);
}

class MotorcyclePart extends Model
{
	//
 	use SoftDeletes;
	protected $fillable = [
		'prod_name',
		'prod_barcode',
		'prod_price',
		'prod_stock',
		'prod_stock_no_unit',
	];

  public function orders_items_by_barcode()
  {
    return $this->hasMany('App\MotorcyclePartsOrderItem','prod_barcode','prod_barcode');
  }
  public function total_orders_items_by_barcode()
  {
    return $this->orders_items_by_barcode->count();
  }
  public function &orders_items_by_barcode_to_calculate_purchase_price()
  {
    $records = $this->hasMany('App\MotorcyclePartsOrderItem','prod_barcode','prod_barcode')
    				->orderBy('id','desc')
    				->select([
						'id',
						'prod_unit_price',
						'order_item_percent_discount',
						'order_item_quantity',
						'order_id'
					]);

    return $records;
  }

	public static function &create_or_update_from_orders(
		$request, 
		$update_stock = false){
		$record = MotorcyclePart::where(
							'prod_barcode',$request->prod_barcode)
							->orWhere('prod_barcode',$request->order_item_code)
							->first();
		if(!$record){
			$record = new MotorcyclePart;
			$record->add_to_stock($request->order_item_quantity, false);
		}else{
			if($update_stock){
				$record->add_to_stock($request->order_item_quantity, false);
			}
		}
		$record->fill($request->all());
		$record->prod_price = is_numeric($request->prod_unit_price_on_sale) ? $request->prod_unit_price_on_sale : $request->prod_unit_price;
		$record->save();
		return $record;
	}

  public function add_to_stock($amount, $by_weight = true){

  	if(!$by_weight){

  		$this->prod_stock += $amount;

  	}else{

  		$total_weight_no_unit = 
  			$this->prod_stock * 1000.0 + 
  			$this->prod_stock_no_unit + 
  			$amount;

			$total_weight = floor( $total_weight_no_unit / 1000.0 );

			$remaining_quantity_weight = $total_weight_no_unit - $total_weight * 1000.0;

			$this->prod_stock = $total_weight;
			$this->prod_stock_no_unit = $remaining_quantity_weight;
  	}

  }

	public function avg_price_from_orders_items_discounting_stock($opt = false){
		$order_items = [];
		$records = $this->orders_items_by_barcode_to_calculate_purchase_price;
		$total_records = $records->count();
		$sum_quantity = 0;
		$sum_prod_prices = 0;
		$total_records_iterated = 0;
		if($total_records > 0){
			if($this->prod_stock>0){

				foreach ($records->all() as &$record) {

					$sum_quantity += $record->order_item_quantity;
					$total_records_iterated += 1;
					//$sum_prod_prices += (float)$record->prod_unit_price;
					$sum_prod_prices += (float)$record->prod_unit_price_with_discount;
					$order_items[] = $record;
					if($sum_quantity >= $this->prod_stock){
						break;
					}

				}

				if($opt === false){
					if($total_records_iterated > 0){
						return $sum_prod_prices / $total_records_iterated;
					}else{
						return null;
					}
				}

			}else{
				if($opt === false)return $records[0]->prod_unit_price_with_discount;
			}
		}else{
			return null;
		}
		if($opt === 1)return $order_items;
	}
}
