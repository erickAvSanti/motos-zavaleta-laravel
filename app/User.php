<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
	use Notifiable;
	use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'password',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
		'is_admin' => 'boolean',
	];
  public function user_role_motorcycle_part()
  {
    return $this->hasOne('App\UserRoleMotorcyclePart','user_id');
  }

  public function user_role_motorcycle_parts_order()
  {
    return $this->hasOne('App\UserRoleMotorcyclePartsOrder','user_id');
  }

  public function user_role_motorcycle_parts_order_item()
  {
    return $this->hasOne('App\UserRoleMotorcyclePartsOrderItem','user_id');
  }

  public function user_role_motorcycle_parts_supplier()
  {
    return $this->hasOne('App\UserRoleMotorcyclePartsSupplier','user_id');
  }

  public function user_role_motorcycle_parts_brand()
  {
    return $this->hasOne('App\UserRoleMotorcyclePartsBrand','user_id');
  }

  public function user_role_technical_staff()
  {
    return $this->hasOne('App\UserRoleTechnicalStaff','user_id');
  }

  public function user_role_user()
  {
    return $this->hasOne('App\UserRoleUser','user_id');
  }

  public function hasRole($role){
  	
  	if($this->is_admin)return true;

  	if(strpos($role,'__') !== false){
  		$flags = explode('__',$role);
  		$user_role = $this["user_role_".$flags[0]];
  		if($user_role){
  			return $user_role[$flags[1]];
  		}
  	}
  	return false;
  }

  public function updateRoles(Request $request){


		DB::transaction(function() use($request){

			if(!$this->user_role_motorcycle_part){
				$role = new \App\UserRoleMotorcyclePart;
				$role->user_id = $this->id;
				$role->save();
				$this->user_role_motorcycle_part()->save($role);
				$this->user_role_motorcycle_part = $role;
			}else{
				$this->user_role_motorcycle_part->resetProps();
			}
			
			if(!$this->user_role_motorcycle_parts_order){
				$role = new \App\UserRoleMotorcyclePartsOrder;
				$role->user_id = $this->id;
				$role->save();
				$this->user_role_motorcycle_parts_order()->save($role);
				$this->user_role_motorcycle_parts_order = $role;
			}else{
				$this->user_role_motorcycle_parts_order->resetProps();
			}
			
			if(!$this->user_role_motorcycle_parts_order_item){
				$role = new \App\UserRoleMotorcyclePartsOrderItem;
				$role->user_id = $this->id;
				$role->save();
				$this->user_role_motorcycle_parts_order_item()->save($role);
				$this->user_role_motorcycle_parts_order_item = $role;
			}else{
				$this->user_role_motorcycle_parts_order_item->resetProps();
			}
			
			if(!$this->user_role_motorcycle_parts_supplier){
				$role = new \App\UserRoleMotorcyclePartsSupplier;
				$role->user_id = $this->id;
				$role->save();
				$this->user_role_motorcycle_parts_supplier()->save($role);
				$this->user_role_motorcycle_parts_supplier = $role;
			}else{
				$this->user_role_motorcycle_parts_supplier->resetProps();
			}

			if(!$this->user_role_user){
				$role = new \App\UserRoleUser;
				$role->user_id = $this->id;
				$role->save();
				$this->user_role_user()->save($role);
				$this->user_role_user = $role;
			}else{
				$this->user_role_user->resetProps();
			}
			
			if(!$this->user_role_motorcycle_parts_brand){
				$role = new \App\UserRoleMotorcyclePartsbrand;
				$role->user_id = $this->id;
				$role->save();
				$this->user_role_motorcycle_parts_brand()->save($role);
				$this->user_role_motorcycle_parts_brand = $role;
			}else{
				$this->user_role_motorcycle_parts_brand->resetProps();
			}
			
			if(!$this->user_role_technical_staff){
				$role = new \App\UserRoleTechnicalStaff;
				$role->user_id = $this->id;
				$role->save();
				$this->user_role_technical_staff()->save($role);
				$this->user_role_technical_staff = $role;
			}else{
				$this->user_role_technical_staff->resetProps();
			}

	  	foreach ($request->all() as $key => &$req) {
				if( preg_match("/^role_/", $key) === 1 ){
					$key = explode("role_",$key);
					$flags = explode("__", $key[1]);

	  			if($flags[0] == 'motorcycle_part'){

	  				$this->user_role_motorcycle_part[$flags[1]] = $req == 1;
	  				$this->user_role_motorcycle_part->save();

	  			}
					
	  			if($flags[0] == 'motorcycle_parts_order'){

	  				$this->user_role_motorcycle_parts_order[$flags[1]] = $req == 1;
	  				$this->user_role_motorcycle_parts_order->save();

	  			}
					
	  			if($flags[0] == 'motorcycle_parts_order_item'){

	  				$this->user_role_motorcycle_parts_order_item[$flags[1]] = $req == 1;
	  				$this->user_role_motorcycle_parts_order_item->save();

	  			}
					
	  			if($flags[0] == 'motorcycle_parts_supplier'){

	  				$this->user_role_motorcycle_parts_supplier[$flags[1]] = $req == 1;
	  				$this->user_role_motorcycle_parts_supplier->save();

	  			}

	  			if($flags[0] == 'user'){

	  				$this->user_role_user[$flags[1]] = $req == 1;
	  				$this->user_role_user->save();

	  			}
					
	  			if($flags[0] == 'motorcycle_parts_brand'){

	  				$this->user_role_motorcycle_parts_brand[$flags[1]] = $req == 1;
	  				$this->user_role_motorcycle_parts_brand->save();

	  			}
					
	  			if($flags[0] == 'technical_staff'){

	  				$this->user_role_technical_staff[$flags[1]] = $req == 1;
	  				$this->user_role_technical_staff->save();

	  			}
				}
	  	}
		});
  }
}
