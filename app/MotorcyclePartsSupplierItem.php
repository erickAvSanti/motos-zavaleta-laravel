<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotorcyclePartsSupplierItem extends Model
{
	//
 	use SoftDeletes;
	protected $fillable = [
		'item_code',
		'prod_name',
		'item_unit',
		'item_type',
		'motorcycle_model_id',
	];

	protected $appends = [
		'motorcycle_model',
	];

	public function getMotorcycleModelAttribute(){
		return $this->belongsTo('\App\MotorcycleModel','motorcycle_model_id')
								->first();
	}

	public function moto_model(){
		return $this->belongsTo('\App\MotorcycleModel','motorcycle_model_id');
	}

	public function supplier_item_on_sales(){
		return $this->hasMany('\App\MotorcyclePartsSupplierItemOnSale','supplier_item_id');
	}

	public static function create_from_supplier_catalog_price(&$row,$extra){
		$record = MotorcyclePartsSupplierItem::where('item_code',$row[0])->first();
		if(!$record){
			$record = new MotorcyclePartsSupplierItem;
		}
		$record->item_code = trim( preg_replace('/\s+/',' ',$row[0]) );
		$record->prod_name = trim( preg_replace('/\s+/',' ',$row[1]) );
		$record->item_unit = trim( preg_replace('/\s+/',' ',$row[2]) );
		$record->item_type = trim( preg_replace('/\s+/',' ',$row[3]) );
		if(@$extra['motorcycle_model']){
			$record->motorcycle_model_id = $extra['motorcycle_model']->id;
		}
		try {
			$record->save();
			return $record;
		} catch (\Exception $e) {
			\Log::info("Error to create supplier item");
			\Log::info($row);
			\Log::info($e->getMessage());
		}
		return null;
		
	}

	public static function create_from_supplier_catalog(&$row,$extra){
		$record = MotorcyclePartsSupplierItem::where('item_code',$row[0])->first();
		if(!$record){
			$record = new MotorcyclePartsSupplierItem;
		}
		$record->item_code = trim( preg_replace('/\s+/',' ',$row[0]) );
		$record->prod_name = trim( preg_replace('/\s+/',' ',$row[1]) );
		$record->item_unit = trim( preg_replace('/\s+/',' ',$row[2]) );
		$record->item_type = trim( preg_replace('/\s+/',' ',$row[3]) );
		if(@$extra['motorcycle_model']){
			$record->motorcycle_model_id = $extra['motorcycle_model']->id;
		}
		if(@$extra['supplier_id']){
			$record->motorcycle_parts_supplier_id = $extra['supplier_id'];
		}
		if(array_key_exists(7, $row) && !empty($row[7])){
			$record->item_currency = trim( preg_replace('/\s+/',' ',$row[6]) );
			$public_price = trim( preg_replace(['/\s+/','/,/'],[' ',''],$row[7]) );
		}else{
			$record->item_currency = trim( preg_replace('/\s+/',' ',$row[5]) );
			$public_price = trim( preg_replace(['/\s+/','/,/'],[' ',''],$row[6]) );
		}
		$record->public_price = $public_price ?? 0;
		
		try {
			$record->save();
		} catch (\Exception $e) {
			\Log::info("Error to create supplier item");
			\Log::info($row);
			\Log::info($e->getMessage());
		}
		
	}

}
