<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class SaleType extends Model
{
	//
	protected $fillable = [
		't_name',
	];
  public function sales()
  {
    return $this->hasMany('App\Sale','sale_type_id');
  }
  public function sales_histories()
  {
    return $this->hasMany('App\SaleHistory','sale_type_id');
  }
	public static function as_options(Request $request){
		$rows = array(['text'=>'Seleccionar tipo de salida', 'value' => null]);
		foreach (SaleType::all() as &$record) {
			$rows[] = [
				'text' => $record->t_name,
				'value' => $record->id,
			];
		}
		return response()->json($rows);
	}
}
