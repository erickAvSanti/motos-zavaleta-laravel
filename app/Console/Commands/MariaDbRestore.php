<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MariaDbRestore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	protected $signature = 'mariadb:restoredb {filename?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restore a specific database file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filename = $this->argument('filename');
        $directory = storage_path('app/mariadb_command_restore/');
        $file_path = "$directory$filename";
        $restore_db_command = "mysql -u".env('DB_USERNAME').( !empty(env('DB_PASSWORD')) ? " -p".env('DB_PASSWORD') : '')." ".env('DB_DATABASE')." < ".$file_path;
        echo "restoring DB";
        echo "Command $restore_db_command";
        $res = shell_exec($restore_db_command);
        echo $res;
        echo "finish";
    }
}
