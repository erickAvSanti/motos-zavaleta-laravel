<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MariaDbBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mariadb:backupdb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a backup from current DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $directory = storage_path('app/mariadb_command_backups/');
        if(!is_dir($directory)){
            mkdir($directory,0777);
        }
        \App\Helpers\MariaDBHelper::make_a_full_db_backup_to_directory($directory);
    }
}
