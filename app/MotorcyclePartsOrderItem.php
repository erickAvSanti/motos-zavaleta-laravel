<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotorcyclePartsOrderItem extends Model
{
	//
	use SoftDeletes;
	protected $fillable = [
		'order_item_code',
		'order_item_percent_discount',
		'order_item_total_price',
		'order_item_quantity',
		'prod_name',
		'prod_barcode',
		'prod_unit_price',
		'prod_unit_price_on_sale',
		'motorcycle_model_id',
		'order_id',
	];
	protected $appends = [
		'motorcycle_model_record',
		'prod_unit_price_with_discount',
	];

	//implement the attribute
	public function getMotorcycleModelRecordAttribute()
	{
	 	return $this->motorcycle_model;
	}
	public function order(){
   		return $this->belongsTo('App\MotorcyclePartsOrder','order_id');
	}
	public function motorcycle_model(){
   		return $this->belongsTo('App\MotorcycleModel','motorcycle_model_id');
	}
	public function getProdUnitPriceWithDiscountAttribute(){
		return $this->prod_unit_price *  ( 100.0 - (float)$this->order_item_percent_discount ) / 100.0;
	}
}
