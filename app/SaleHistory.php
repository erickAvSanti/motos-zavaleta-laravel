<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleHistory extends Model
{
	//
	protected $fillable = [
		'sale_id',
		'sale_type_id',
		'technical_staff_id',
		'sale_total',
		'sale_created_at',
		'sale_updated_at',
		'json_products',
		'issued_at',
	];

	protected $dates = [
		//'issued_at',
	];
	
	public function sale()
	{
		return $this->belongsTo('App\Sale','sale_id');
	}
		public function sale_type()
	{
		return $this->belongsTo('App\SaleType','sale_type_id');
	}
		public function technical_staff()
	{
		return $this->belongsTo('App\TechnicalStaff','technical_staff_id');
	}

	public static function create_for(\App\Sale $sale){

		$tmp = $sale->toArray();
		$tmp['sale_id'] = $sale->id;
		$tmp['sale_type_id'] = $sale->sale_type_id;
		$tmp['technical_staff_id'] = $sale->technical_staff_id;
		$tmp['sale_created_at'] = $sale->created_at;
		$tmp['sale_updated_at'] = $sale->updated_at;
		if($sale->issued_at)$tmp['issued_at'] = $sale->issued_at;
		$sale_history = new SaleHistory($tmp);
		
		try {
			$sale_history->save();
		} catch (\Exception $e) {
			\Log::info($e->getMessage());
			throw $e;
		}
		return $sale_history;
	}
}
