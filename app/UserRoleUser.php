<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRoleUser extends Model
{
	//
	protected $fillable = [
		'user_id',
		'list_records',
		'view_record',
		'update_record',
		'create_record',
		'delete_record',
		'export_excel',
	];
	protected $casts = [
		'list_records' => 'boolean',
		'view_record' => 'boolean',
		'update_record' => 'boolean',
		'create_record' => 'boolean',
		'delete_record' => 'boolean',
		'export_excel' => 'boolean',
	];
	const CASTS_DESCRIPTION = [
		'list_records' => 'Listar usuarios',
		'view_record' => 'Ver usaurio',
		'update_record' => 'Actualizar usuario',
		'create_record' => 'Crear usuario',
		'delete_record' => 'Eliminar usuario',
	];
	public function getCasts(){
		return $this->casts;
	}
	public function user()
  {
   	return $this->belongsTo('App\User','user_id');
  }
  public function resetProps(){
  	foreach (self::CASTS_DESCRIPTION as $key => &$value) {
  		$this[$key] = false;
  	}
  	$this->save();
  }
}
