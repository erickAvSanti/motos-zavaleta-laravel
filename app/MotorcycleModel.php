<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotorcycleModel extends Model
{
	//
	use SoftDeletes;
	protected $fillable = [
		'mo_name',
	];

	public function supplier_items(){
		return $this->hasMany('\App\MotorcyclePartsSupplierItem','motorcycle_model_id');
	}
	public function motorcycle_parts_order_items(){
    return $this->hasMany(
    	'App\MotorcyclePartsOrderItem',
    	'motorcycle_model_id');
	}

	public static function create_from_name($name){
		if(!empty($name)){
			$name = trim(preg_replace("/ +/"," ",$name));
			if(empty($name))return null;
		}else{
			return null;
		}
		$record = MotorcycleModel::where('mo_name',$name)->first();
		if(!$record){
			$record = new MotorcycleModel;
			$record->mo_name = $name;
			$record->save();
		}
		return $record;
	}
}
