<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next, $role)
	{
		if(!$request->user()){
			if($request->wantsJson()){
	      return response()->json(['msg'=>"user undefined"],419);
	    }else{
	      abort(419);
	    }
		}else{
			if($request->user()->hasRole($role)){
				return $next($request);
			}else{
				if($request->wantsJson()){
		      return response()->json(['msg'=>"missing role"],403);
		    }else{
		      abort(403);
		    }
			}
		}
	}
}
