<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\MotorcyclePartsOrder;
use \App\MotorcyclePartsOrderItem;
use \App\MotorcyclePart;
use \App\MotorcycleModel;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class MotorcyclePartsOrdersController extends Controller
{
	//
	const ROWS_X_PAGES = 20;
  public function __construct()
  {
      $this->middleware('auth');
  }

  public static function show(){

  	return view('motorcycle_parts_orders.index');

  }

  public function create_or_update(Request $request){
		try{
			if( $request->id ){
				$record = MotorcyclePartsOrder::find( $request->id );
			}else{
				$record = new MotorcyclePartsOrder;
			}
			$record->fill($request->all());
			$record->save();
			return response()->json($record);
		}catch(\Exception $e){
			\Log::info(" create_or_update, msg= " . $e->getMessage());
			return response()->json(['msg' => $e->getMessage()],500);
		}
  }

  public function list(Request $request){

  	/*
  	if($request->extra_cond === 'distinct_items'){
  		return response()->json(MotorcyclePartsOrderItem::select('prod_barcode')->distinct()->count());
  	}
  	*/

		$page = 
			is_numeric($request->page) ? $request->page : 1;
		$rows_x_page = 
			$request->rows_x_page && 
			is_numeric($request->rows_x_page) ? 
			$request->rows_x_page : self::ROWS_X_PAGES;

		$order_by = $request->order_by ?? '';
		$order = $request->order ?? '';

		$records = new MotorcyclePartsOrder;

		if(!empty($request->query_string)){
			$records = $records->where(function ($query) use ($request) {
               $query->where('order_code','LIKE',"%".$request->query_string."%");
           });
			//$records = Product::where('prod_name','LIKE',"%".$request->input('query')."%")->orWhere('prod_barcode','LIKE',"%".$request->input('query')."%");
			if(in_array($order,['asc','desc'])){
				$records = $records->orderBy($order_by,$order);
			}
		}else{
			if(!empty($order_by)){
				$records = $records->orderBy($order_by,$order);
			}
		}
		if($request->show_deleted == 1){
			$records = $records->onlyTrashed();
		}
		if(!empty($request->order_item_code)){
			if($request->show_deleted == 1){
				$records = $records->whereHas('items_deleted', function($query) use($request){
					$query->where('order_item_code',$request->order_item_code);
				});
			}else{
				$records = $records->whereHas('items', function($query) use($request){
					$query->where('order_item_code',$request->order_item_code);
				});
			}
		}
		if(!empty($request->order_code)){
			$records = $records->where('order_code',$request->order_code);
		}
		if(!empty($request->order_invoice)){
			$records = $records->where('order_invoice',$request->order_invoice);
		}

		$bought_at = $request->bought_at ?? '';

		if( 
			$bought_at && 
			preg_match("/\d{2}\/\d{2}\/\d{4}/", $bought_at) === 1 
		){
			$bought_at 	= Carbon::createFromFormat("!d/m/Y",$bought_at)
													->toDateTimeString();

			$records = $records->whereDate('bought_at','=',$bought_at);
		}

		if($request->extra_cond === 'distinct_items'){
			$records = $records->join('motorcycle_parts_order_items','motorcycle_parts_orders.id','=','motorcycle_parts_order_items.order_id')
											->select('motorcycle_parts_order_items.prod_barcode')
											->distinct('motorcycle_parts_order_items.prod_barcode');
			\Log::info($records->toSql());

			$total = $records->count();

			return response()->json($total);

		}


		$total_records = $records->count();
		$total_pages = floor($total_records / $rows_x_page) + 1;
		$page = $page < 1 ? 1 : ( 
			$total_pages < $page ? $total_pages : $page 
		);

		\Log::info($records->toSql());

		$records = $records->offset( 
			$rows_x_page * ( $page - 1 ) 
		)->limit( $rows_x_page );

		return response()->json([
			'records' => $records->get(), 
			'query_string' => ( $request->query_string ?? '' ),
			'order_by' => $order_by,
			'order' => $order,
			'totalRows' => $total_records,
			'totalPages' => $total_pages,
			'rowsPerPage' => $rows_x_page,
			'page' => $page,
			'show_deleted' => $request->show_deleted,
		]);
  }

  public function list_items(Request $request, $id){
  	if($request->show_deleted == 1){
  		$records = MotorcyclePartsOrder::find($id)->items_deleted;
  	}else{
  		$records = MotorcyclePartsOrder::find($id)->items;
  	}
  	return response()->json($records);
  }

  public function create_or_update_item(Request $request){
		try{
			if( !$request->order_id ) throw new \Exception('undefined order ID at request variable');
			if( $request->id ){
				$record = MotorcyclePartsOrderItem::find( $request->id );
			}else{
				$record = new MotorcyclePartsOrderItem;
			}
			DB::transaction(function() use($record, $request){
				$record->fill($request->all());
				$record->save();

				$motorcycle_part = &MotorcyclePart::create_or_update_from_orders($request, $request->add_to_current_stock != 'no');

			});
			return response()->json($record);
		}catch(\Exception $e){
			\Log::info(" create_or_update_item, msg= " . $e->getMessage());
			return response()->json(['msg' => $e->getMessage()],500);
		}
  }
  public function list_moto_models(Request $request){
  	if($request->to_options==1){
  		$rows = [['text' => 'Seleccionar', 'value' => null]];
  		foreach (MotorcycleModel::orderBy('mo_name','asc')->get() as &$record) {
  			$rows[] = [
  				'text' => $record->mo_name,
  				'value' => $record->id,
  			];
  		}
  		return response()->json($rows);
  	}else{
  		$records = new MotorcycleModel;
  		$records = $records->orderBy('mo_name','asc');
  		if($request->show_deleted == 1){
  			$records = $records->onlyTrashed();
  		}

			$page = 
				is_numeric($request->page) ? $request->page : 1;

			$rows_x_page = 
				$request->rows_x_page && 
				is_numeric($request->rows_x_page) ? 
				$request->rows_x_page : self::ROWS_X_PAGES;

			$request->search_value = trim($request->search_value ?? '');
			if(!empty($request->search_value)){
				$records = $records->where('mo_name','LIKE',"%".$request->search_value."%");
			}
			
			$total_records = $records->count();
			$total_pages = floor($total_records / $rows_x_page) + 1;
			$page = $page < 1 ? 1 : ( 
				$total_pages < $page ? $total_pages : $page 
			);

			\Log::info($records->toSql());

			$records = $records->offset( 
				$rows_x_page * ( $page - 1 ) 
			)->limit( $rows_x_page );

			return response()->json([
				'records' => $records->get(),
				'totalRows' => $total_records,
				'totalPages' => $total_pages,
				'rowsPerPage' => $rows_x_page,
				'page' => $page,
			]);

  	}
  }

  public function create_or_update_motorcycle_models(Request $request){
		try{
			if( $request->id ){
				$record = MotorcycleModel::find( $request->id );
			}else{
				$record = new MotorcycleModel;
			}
			$record->fill($request->all());
			$record->save();
			return response()->json($record);
		}catch(\Exception $e){
			\Log::info(" create_or_update_motorcycle_models, msg= " . $e->getMessage());
			return response()->json(['msg' => $e->getMessage()],500);
		}
  }
	public function delete($id)
	{
		$record = MotorcyclePartsOrder::withTrashed()->find($id);
		if($record->trashed()){
			$deleted = $record->restore();
		}else{
			$deleted = $record->delete();
		}
		return response()->json($record);
	}
	public function delete_item($id)
	{
		$record = MotorcyclePartsOrderItem::withTrashed()->find($id);
		if($record->trashed()){
			$deleted = $record->restore();
		}else{
			$deleted = $record->delete();
		}
		return response()->json($record);
	}
	public function delete_motorcycle_model($id)
	{
		$record = MotorcycleModel::withTrashed()->find($id);
		if($record->trashed()){
			$deleted = $record->restore();
		}else{
			$deleted = $record->delete();
		}
		return response()->json($record);
	}
	public function excel_report(Request $request)
	{
		return \Redirect::to(MotorcyclePartsOrder::gen_excel_report_by($request));
	}

}
