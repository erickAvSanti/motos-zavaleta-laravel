<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\SaleType;

class SaleTypesController extends Controller
{
	//
  public function __construct()
  {
 		$this->middleware('auth');
  }
  public function index(Request $request)
  {
    if($request->values_as == 'options'){
      return SaleType::as_options($request);
    }
    return response()->json(SaleType::all());
  }
	public function create(Request $request)
	{
		try {
			($record = new SaleType)->fill($request->all())->save();
			return response()->json($record);
		} catch (\Exception $e) {
			\Log::info($e->getTraceAsString());
			return response()->json([],500);
		}
	}
	public function update(Request $request, $id)
	{
		$record = SaleType::find($id);
		if(!$record){
			return response()->json([],404);
		}else{
			try {
				$record->fill($request->all())->save();
				return response()->json($record);
			} catch (\Exception $e) {
				\Log::info($e->getTraceAsString());
				return response()->json([],500);
			}
		}
	}
  public function delete(Request $request, $id)
  {
    try {
    	$record = TechnicalStaff::withTrashed()->find($id);
	    if(!$record){
	    	return response()->json([],404);
	    }else{
	    	if($record->trashed())$record->restore();
	    	else $record->delete();
	    }
	    return response()->json($record);
    } catch (\Exception $e) {
    	\Log::info($e->getTraceAsString());
    	return response()->json([],500);
    }
  }
}
