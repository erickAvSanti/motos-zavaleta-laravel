<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\MotorcyclePartsSupplierItemOnSale;
use Illuminate\Support\Facades\DB;

class MotorcyclePartsSupplierItemOnSaleController extends Controller
{
	//
	const ROWS_X_PAGES = 20;
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index(Request $request){
  	return view('supplier_item_on_sales');
  }

	public function create(Request $request){

		try{
			$record = MotorcyclePartsSupplierItemOnSale::create_from_request_props($request);
			return response()->json($record->toCustomArray()); 
		}catch(\Exception $e){
			\Log::info($e->getMessage());
			return response()->json([],500); 
		}

	}

	public function update(Request $request, $id){
		try{
			$record = MotorcyclePartsSupplierItemOnSale::update_from_request_props($request,$id);
			return response()->json($record->toCustomArray()); 
		}catch(\Exception $e){
			return response()->json([],500); 
		}

	}

  public function list(Request $request){
  	
		$page = 
			is_numeric($request->page) ? $request->page : 1;
		$rows_x_page = 
			$request->rows_x_page && 
			is_numeric($request->rows_x_page) ? 
			$request->rows_x_page : self::ROWS_X_PAGES;

		$order_by = $request->order_by ?? '';
		$order = $request->order ?? '';
		$search_value = trim($request->search_value) ?? '';

		$records = new MotorcyclePartsSupplierItemOnSale;

		if(in_array($order,['asc','desc'])){
			$records = $records->orderBy($order_by,$order);
		}

		if($request->show_deleted == 1){
			$records = $records->onlyTrashed();
		}

		if(is_numeric($request->brand_id)){
			$records = $records->where('brand_id',$request->brand_id);
		}

		if(!empty($search_value)){
			$records = $records->join(
											'motorcycle_parts_supplier_items',
											'motorcycle_parts_supplier_items.id',
											'=',
											'motorcycle_parts_supplier_item_on_sales.supplier_item_id'
										)->where(function($query) use($search_value){
											$query->where(
												'motorcycle_parts_supplier_items.prod_name',
												'LIKE',
												"%".$search_value."%"										
											)->orWhere(
												'motorcycle_parts_supplier_items.item_code',
												'LIKE',
												"%".$search_value."%"		
											);
										});
		}

		if(!empty($request->item_currency)){
			$records = $records->where('item_currency',$request->item_currency=='Vacío' ? '' : $request->item_currency);			
		}

		


		$total_records = $records->count();
		$total_pages = floor($total_records / $rows_x_page) + 1;
		$page = $page < 1 ? 1 : ( 
			$total_pages < $page ? $total_pages : $page 
		);

		\Log::info($records->toSql());

		$records = $records->offset( 
			$rows_x_page * ( $page - 1 ) 
		)->limit( $rows_x_page );

		return response()->json([
			'records' => $records->get(), 
			'query_string' => ( $request->query_string ?? '' ),
			'order_by' => $order_by,
			'order' => $order,
			'totalRows' => $total_records,
			'totalPages' => $total_pages,
			'rowsPerPage' => $rows_x_page,
			'page' => $page,
			'show_deleted' => $request->show_deleted,
			'currency_options' => MotorcyclePartsSupplierItemOnSale::currency_options(),
		]);


	}

  public function delete(Request $request, $id)
  {
    try {
    	$record = MotorcyclePartsSupplierItemOnSale::withTrashed()->find($id);
	    if(!$record){
	    	return response()->json([],404);
	    }else{
	    	if($record->trashed())$record->restore();
	    	else $record->delete();
	    }
	    return response()->json($record);
    } catch (\Exception $e) {
    	\Log::info($e->getTraceAsString());
    	return response()->json([],500);
    }
  }
  
}
