<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\MotorcyclePart;

use \App\Helpers\MotorcyclePartHelper;
class MotorcyclePartsController extends Controller
{
	//
  const ROWS_X_PAGES = 20;
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function show(Request $request, $id)
  {
    $record = MotorcyclePart::find($id);
    if(
      !$record && 
      !empty($request->prod_barcode)
    ){
      $record = MotorcyclePart::where('prod_barcode', $request->prod_barcode)->first();
    }
    if($request->wantsJson()){
      return response()->json($record);
    }else{
      return view('products.form_edit',[
        'record' => $record,
        'id' => $id,
      ]);
    }
  }
  public function index(Request $request)
  {

    $page = 
      is_numeric($request->page) ? $request->page : 1;
    $rows_x_page = 
      $request->rows_x_page && 
      is_numeric($request->rows_x_page) ? 
      $request->rows_x_page : self::ROWS_X_PAGES;

    $order_by = $request->input('order_by') ?? '';
    $order = $request->input('order') ?? '';
    if(!empty($request->input('query'))){
      $records = MotorcyclePart::where(function ($query) use ($request) {
               $query->where('prod_name','LIKE',"%".$request->input('query')."%")
               ->orWhere('prod_barcode','LIKE',"%".$request->input('query')."%");
           });
      //$records = MotorcyclePart::where('prod_name','LIKE',"%".$request->input('query')."%")->orWhere('prod_barcode','LIKE',"%".$request->input('query')."%");
      if(in_array($order,['asc','desc'])){
        $records = $records->orderBy($order_by,$order);
      }
    }else{
      if(empty($order_by) && empty($order)){
        $records = MotorcyclePart::select('*');
      }else{
        if(!empty($order_by)){
          $records = MotorcyclePart::orderBy($order_by,$order);
        }
      }
    }
    if(isset($request->show_deleted)){
      $records = $records->onlyTrashed();
    }
    if(isset($request->show_who_have_acquisitions)){
      $records = $records->whereHas('orders_items_by_barcode');
    }
    $total_records = $records->count();
    $total_pages = floor($total_records / $rows_x_page) + 1;
    $page = $page < 1 ? 1 : ( 
      $total_pages < $page ? $total_pages : $page 
    );

    \Log::info($records->toSql());

    $records = $records->offset( 
      $rows_x_page * ( $page - 1 ) 
    )->limit( $rows_x_page );

    return view('products',[
      'records' => $records->get(), 
      'query' => ( $request->input('query') ?? '' ),
      'order_by' => $order_by,
      'order' => $order,
      'totalRows' => $total_records,
      'totalPages' => $total_pages,
      'rowsPerPage' => $rows_x_page,
      'page' => $page,
      'show_deleted' => $request->show_deleted,
      'show_who_have_acquisitions' => $request->show_who_have_acquisitions,
    ]);
  }

  public static function info(Request $request){

  	if(empty($request->barcode)){
  		return response()->json([
  			'msg' => 'barcode input undefined'],400);
  	}else{
  		$record = MotorcyclePart::where(
  			'prod_barcode',$request->barcode)->first();
  		if($record){
  			return response()->json($record);	
  		}else{
  			return response()->json(['msg'=>'not found'],404);
  		}
  		
  	}

  }
  public function update(Request $request)
  {
    $record = MotorcyclePart::find($request->id);
    $record->fill($request->all())->save();

    return view('products.form_success_updated',[
      'record' => $record,
    ]);
  }
  public function delete($id)
  {
    $record = MotorcyclePart::withTrashed()->find($id);
    $deleted = false;
    $is_trashed = false;
    if($record){
      if($record->trashed()){
        $is_trashed = true;
        $deleted = $record->restore();
      }else{
        $deleted = $record->delete();
      }
    }

    return view('products.delete_status',[
      'record' => $record,
      'deleted' => $deleted,
      'is_trashed' => $is_trashed,
    ]);
  }

  public function filter_by(Request $request){

    if(isset($request->type) && $request->type == 'prod_barcode' && !empty($request->filter_field_value)){
      return response()->json([
        'records' => MotorcyclePart::where('prod_barcode','=',"$request->filter_field_value")->get()->toArray(),
      ]);
    }

    if(isset($request->type) && $request->type == 'prod_name' && !empty($request->filter_field_value)){
      return response()->json([
        'records' => MotorcyclePart::where('prod_name','LIKE',"%$request->filter_field_value%")->get()->toArray(),
      ]);
    }
    return response()->json([
      'records' => [],
    ]);
  }
  public function excel_export_all(){
    return response()->file(MotorcyclePartHelper::excel_export_all());
  }

}
