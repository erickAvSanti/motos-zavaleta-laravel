<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\TechnicalStaff;

class TechnicalStaffController extends Controller
{
	//
  const ROWS_X_PAGES = 20;
  public function __construct()
  {
    $this->middleware('auth');
  }
  public function add(Request $request)
  {
    $props = $request->all();
    return view('technical_staff.form_add',['props' => $props]);
  }
  public function show(Request $request, $id)
  {
    $record = TechnicalStaff::find($id);
    if($request->wantsJson()){
      if($record){
      	return response()->json($record);
      }else{
      	return response()->json([],404);
      }
    }else{
    	if($record){
	      return view('technical_staff.form_edit',[
	        'record' => $record,
	        'id' => $id,
	      ]);
	    }else{
	    	abort(404);
	    }
    }
  }
  public function index(Request $request)
  {
    if($request->values_as == 'options'){
      return TechnicalStaff::as_options($request);
    }
    $page = 
      is_numeric($request->page) ? $request->page : 1;

    $rows_x_page = 
      $request->rows_x_page && 
      is_numeric($request->rows_x_page) ? 
      $request->rows_x_page : self::ROWS_X_PAGES;

    $order_by = $request->order_by ?? '';
    $order 		= $request->order ?? '';

    $records = new TechnicalStaff;

    if(!empty($request->query_value)){

      $records = 	$records->where(function ($query) use ($request) {
               			$query->where('staff_name','LIKE',"%".$request->query_value."%");
           				});

      if(in_array($order,['asc','desc'])){
        $records = $records->orderBy($order_by,$order);
      }

    }else{
      if(!empty($order_by)){
        $records = $records->orderBy($order_by,$order);
      }
    }
    if(isset($request->show_deleted)){
      $records = $records->onlyTrashed();
    }
    $total_records = $records->count();
    $total_pages = floor($total_records / $rows_x_page) + 1;
    $page = $page < 1 ? 1 : ( 
      $total_pages < $page ? $total_pages : $page 
    );

    \Log::info($records->toSql());

    $records = $records->offset( 
      $rows_x_page * ( $page - 1 ) 
    )->limit( $rows_x_page );

    return view('technical_staff',[
      'records' 			=> $records->get(), 
      'query_value' 	=> $request->query_value,
      'order_by' 			=> $order_by,
      'order' 				=> $order,
      'totalRows' 		=> $total_records,
      'totalPages' 		=> $total_pages,
      'rowsPerPage' 	=> $rows_x_page,
      'page' 					=> $page,
      'show_deleted' 	=> $request->show_deleted,
    ]);
  }
  public function create(Request $request)
  {
    $record = new TechnicalStaff;
    try {
      $record->fill($request->all())->save();
      if($request->wantsJson()){
        return response()->json($record);
      }else{
        return view('technical_staff.form_success_created',[
          'record' => $record,
        ]);
      }
    } catch (\Exception $e) {
      \Log::info($e->getMessage());
      \Log::info($e->getTraceAsString());
      if($request->wantsJson())return response()->json([],500);
      else abort(500);
    }
  }
  public function update(Request $request, $id)
  {
    $record = TechnicalStaff::find($request->id);
    if($record){
      try {
        $record->fill($request->all())->save();
        if($request->wantsJson()){
          return response()->json($record);
        }else{
          return view('technical_staff.form_success_updated',[
            'record' => $record,
          ]);
        }
      } catch (\Exception $e) {
        \Log::info($e->getTraceAsString());
        if($request->wantsJson())return response()->json([],500);
        else abort(500);
      }

    }else{
      if($request->wantsJson())return response()->json([],404);
      else abort(404);
    }
  }
  public function delete(Request $request, $id)
  {
    $record = TechnicalStaff::withTrashed()->find($id);
    $deleted = false;
    $is_trashed = false;
    if($record){
      if($record->trashed()){
        $is_trashed = true;
        $deleted = $record->restore();
      }else{
        $deleted = $record->delete();
      }
    }

    if($record){
    	$res = [
		      'record' => $record,
		      'deleted' => $deleted,
		      'is_trashed' => $is_trashed,
		    ];
    	if($request->wantsJson())return response()->json($res);
    	else return view('technical_staff.delete_status',$res);
    }else{
    	if($request->wantsJson())return response()->json([],404);
    	else abort(404);
    }
  }
  public function filter_by(Request $request){
    $query_value = $request->query_value ?? '';
    if($query_value != ''){
      return response()->json(
        TechnicalStaff::where('staff_name','LIKE',"%$query_value%")
                      ->get());
    }
    return response()->json([]);
  }
}
