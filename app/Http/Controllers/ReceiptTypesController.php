<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\ReceiptType;

class ReceiptTypesController extends Controller
{
	//
	public function __construct()
	{
		$this->middleware('auth');
	}
	public function show(Request $request){
		if($request->wantsJson()){
			
  		if($request->to_options==1){
	  		$rows = [['text' => 'Seleccionar', 'value' => null]];
	  		foreach (ReceiptType::all() as &$record) {
	  			$rows[] = [
	  				'text' => $record->rt_name,
	  				'value' => $record->id,
	  			];
	  		}
	  		return response()->json($rows);
	  	}else{
				$records = $request->show_deleted == 1 ? ReceiptType::onlyTrashed()->get() : ReceiptType::all(); 
	  		return response()->json($records);
	  	}

		}else{
			return null;
		}
	}
	public function create_or_update(Request $request){
		try{
			if( $request->id ){
				$record = ReceiptType::find( $request->id );
			}else{
				$record = new ReceiptType;
			}
			$record->fill($request->all());
			$record->save();
			return response()->json($record->toArray());
		}catch(\Exception $e){
			\Log::info(" create_or_update");
			\Log::info($e->getMessage());
			return response()->json(['msg' => $e->getMessage()],500);
		}
	}
	public function delete($id)
	{
		$record = ReceiptType::withTrashed()->find($id);
		if($record->trashed()){
			$deleted = $record->restore();
		}else{
			$deleted = $record->delete();
		}
		return response()->json($record);
	}
}
