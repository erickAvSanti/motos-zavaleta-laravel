<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\MotorcyclePartsBrand;

class MotorcyclePartsBrandController extends Controller
{
	//
  const ROWS_X_PAGES = 20;
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function list(Request $request){
  	if($request->values_as === 'options'){
  		$props = &MotorcyclePartsBrand::as_options($request);
  		return response()->json($props);
  	}
  }
}
