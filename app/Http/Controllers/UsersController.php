<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use \App\User;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index(Request $request)
	{
		$order_by = $request->input('order_by') ?? '';
		$order = $request->input('order') ?? '';
		if(!empty($request->input('query'))){
			$records = User::where(function ($query) use ($request) {
               $query->where('name','LIKE',"%".$request->input('query')."%");
           });
			//$records = User::where('name','LIKE',"%".$request->input('query')."%");
			if(in_array($order,['asc','desc'])){
				$records = $records->orderBy($order_by,$order);
			}
		}else{
			if(empty($order_by) && empty($order)){
				$records = User::orderBy('created_at','desc');
			}else{
				if(!empty($order_by)){
					$records = User::orderBy($order_by,$order);
				}
			}
		}
		if(isset($request->show_deleted)){
			$records = $records->onlyTrashed();
		}
		return view('users',[
			'records' => $records->get(), 
			'query' => ( $request->input('query') ?? '' ),
			'order_by' => $order_by,
			'order' => $order,
			'show_deleted' => $request->show_deleted,
		]);
	}
	public function show(Request $request, $id)
	{
		$record = User::find($id);
		return view('users.form_edit',[
			'record' => $record,
			'id' => $id,
		]);
	}
	public function add(Request $request)
	{
		$props = $request->all();
		return view('users.form_add',['props' => $props]);
	}
	public function create(UserRequest $request)
	{
		$validated = $request->validated();
		//$props = $request->all();
		$record = new User();
		if($request->password){
			$record->password = Hash::make($request->password);
		}
		$record->email = $request->email;
		$record->name = $request->name;
		$record->is_admin = $request->is_admin;
		try {
			$record->save();
			$record->updateRoles($request);
		} catch (\Exception $e) {
			\Log::info($e->getTraceAsString());
			$record = null;
		}

		return view('users.form_success_created',[
			'record' => $record,
		]);
	}
	public function update(UserRequest $request)
	{
		$validated = $request->validated();
		$record = User::find($request->id);
		if($record){
			if($request->password){
				$record->password = Hash::make($request->password);
			}
			$record->email = $request->email;
			$record->name = $request->name;
			$record->is_admin = $request->is_admin;
			try {
				$record->save();
				$record->updateRoles($request);
			} catch (\Exception $e) {
				\Log::info($e->getTraceAsString());
				$record = null;
			}
		}
		return view('users.form_success_updated',[
			'record' => $record,
		]);
	}
	public function delete($id)
	{

		$current_user = auth()->user();
		if(!$current_user->is_admin)abort(403);

		$record = User::withTrashed()->find($id);
		$deleted = false;
		$is_trashed = false;
		if($record){
			if($record->id != $current_user->id){
				if($record->trashed()){
					$is_trashed = true;
					$deleted = $record->restore();
				}else{
					$deleted = $record->delete();
				}
			}else{
				
			}
		}

		return view('users.delete_status',[
			'record' => $record,
			'deleted' => $deleted,
			'is_trashed' => $is_trashed,
		]);
	}
}