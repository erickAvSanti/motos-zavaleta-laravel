<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		$user = auth()->user();
		return $user->is_admin;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$arr = [
			//
			'name' => 'required',
			'email' => 'required|unique:users,email,'.$this->id,
		];
		if(
			!empty(trim(@$this->password)) && 
			!empty(trim(@$this->password_confirmation)) && 
			isset($this->id)
		){
			$arr['password'] = 'required|string|min:8|max:20';
			$arr['password_confirmation'] = 'same:password';
		}
		return $arr;
	}
	public function messages()
	{
	  $arr = [
      'user_name.required' => 'El usuario es requerido',
      'email.required' => 'El email es requerido',
      'email.unique' => 'El email ya existe',
      ];

		if(
			!empty(trim(@$this->password)) && 
			!empty(trim(@$this->password_confirmation)) && 
			isset($this->id)
		){
			$arr['password.required'] = 'La contraseña es requerida';
			$arr['password.string'] = 'La contraseña debe ser una cadena de caracteres alfanumericos';
			$arr['password.min'] = 'La mínima longitud de caracteres de la contraseña es 8';
			$arr['password.max'] = 'La máxima longitud de caracteres de la contraseña es 30';
			$arr['password_confirmation.required'] = 'La confirmación de la contraseña es requerida';
			$arr['password_confirmation.same'] = 'La contraseña y la confirmación de la contraseña deben ser las mismas';
		}

		return $arr;

	}

	/**
	* Get custom attributes for validator errors.
	*
	* @return array
	*/
	public function attributes()
	{
	  return [
	  	'name' => 'Nombre del usuario',
	  	'email' => 'Correo electrónico',
	  	'password' => 'Contraseña',
	  	'password_confirmation' => 'Confirmación de contraseña',
	  ];
	}
	/**
	* Prepare the data for validation.
	*
	* @return void
	*/
	protected function prepareForValidation()
	{
		$arr = [];
		if(!$this->is_admin)
		{
			$arr['is_admin'] = false;
		}
		if(
			$this->is_admin != 0 && 
			$this->is_admin != 1
		){
			$arr['is_admin'] = false;
		}
		$this->merge($arr);
	}
}
