<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceiptType extends Model
{
	//
	use SoftDeletes;
	protected $fillable = [
		'rt_name'
	];
	public function orders(){
    return $this->hasMany('App\MotorcyclePartsOrder','order_receipt_type_id');
	}
}
