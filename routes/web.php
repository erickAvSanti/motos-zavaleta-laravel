<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return redirect()->route('home');
});

$route_options = [];
if(app()->environment() !== 'local'){
  $route_options['register'] = false;
  $route_options['reset'] = false;
  $route_options['confirm'] = false;
  $route_options['verify'] = false;
}
Auth::routes($route_options);

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => ['cors']], function () {
    //Rutas a las que se permitirá acceso
});


Route::prefix('usuarios')->group(function () {
  Route::get('/', 'UsersController@index')
        ->name('users')
        ->middleware('role:user__list_records');

  Route::get('/nuevo', 'UsersController@add')
        ->name('users_new')
        ->middleware('role:user__create_record');

  Route::get('/{id}', 'UsersController@show')
        ->name('users_show')
        ->where('id', '\d+')
        ->middleware('role:user__view_record');

  Route::post('/eliminar/{id}', 'UsersController@delete')
        ->name('users_delete')
        ->where('id', '\d+')
        ->middleware('role:user__delete_record');

  Route::post('/crear', 'UsersController@create')
        ->name('users_create')
        ->middleware('role:user__create_record');

  Route::post('/{id}', 'UsersController@update')
        ->name('users_update')
        ->where('id', '\d+')
        ->middleware('role:user__update_record');
});

Route::group(['prefix' => 'pedidos-de-repuestos-de-motos'], function(){
  Route::get('/', 'MotorcyclePartsOrdersController@show')->name('motorcycle_parts_orders');
    Route::get('/exportar-excel-todo', 'MotorcyclePartsOrdersController@excel_report');
  Route::get('/list', 'MotorcyclePartsOrdersController@list');
  Route::post('/', 'MotorcyclePartsOrdersController@create_or_update')->name('motorcycle_parts_orders_create_or_update');
  Route::delete('/{id}', 'MotorcyclePartsOrdersController@delete')->where('id', '\d+');

  Route::group(['prefix' => 'items'], function(){
    Route::get('/{id}','MotorcyclePartsOrdersController@list_items')->where('id', '\d+');
    Route::post('/','MotorcyclePartsOrdersController@create_or_update_item');
    Route::delete('/{id}','MotorcyclePartsOrdersController@delete_item')->where('id', '\d+');
  });
  Route::group(['prefix' => 'modelos-de-motos'], function(){
    Route::get('/','MotorcyclePartsOrdersController@list_moto_models')->name('motorcycle_models');
    Route::post('/','MotorcyclePartsOrdersController@create_or_update_motorcycle_models');
    Route::delete('/{id}','MotorcyclePartsOrdersController@delete_motorcycle_model')->where('id', '\d+');
  });

});

Route::group(['prefix' => 'proveedores'], function(){
  Route::get('/', 'MotorcyclePartsSuppliersController@show')->name('motorcycle_part_suppliers');
  Route::post('/', 'MotorcyclePartsSuppliersController@create_or_update')->name('motorcycle_part_suppliers_create_or_update');
  Route::delete('/{id}', 'MotorcyclePartsSuppliersController@delete')->where('id', '\d+');

});

Route::group(['prefix' => 'tipos-de-recibos'], function(){
  Route::get('/', 'ReceiptTypesController@show')->name('receipt_types');
  Route::post('/', 'ReceiptTypesController@create_or_update')->name('receipt_types_create_or_update');
  Route::delete('/{id}', 'ReceiptTypesController@delete')->where('id', '\d+');

});

Route::group(['prefix' => 'productos'], function(){

  Route::get('/info', 'MotorcyclePartsController@info');
  
  Route::post('/', 'MotorcyclePartsController@create_or_update')
        ->name('motorcycle_parts_create_or_update');
  
  Route::delete('/{id}', 'MotorcyclePartsController@motorcycle_parts_delete')
        ->where('id', '\d+');


  Route::get('/', 'MotorcyclePartsController@index')
        ->name('motorcycle_parts')
        ->middleware('role:motorcycle_part__list_records');

  Route::get('/{id}', 'MotorcyclePartsController@show')
        ->name('motorcycle_parts_show')
        ->where('id', '\d+')
        ->middleware('role:motorcycle_part__view_record');

  Route::get('/filtrar_por', 'MotorcyclePartsController@filter_by')
        ->name('motorcycle_parts_filter_by');

  Route::post('/eliminar/{id}', 'MotorcyclePartsController@delete')
        ->name('motorcycle_parts_delete')
        ->where('id', '\d+')
        ->middleware('role:motorcycle_part__delete_record');

  Route::post('/{id}', 'MotorcyclePartsController@update')
        ->name('motorcycle_parts_update')
        ->where('id', '\d+')
        ->middleware('role:motorcycle_part__update_record');

  Route::post('/importar-desde-csv', 'MotorcyclePartsController@import_from_csv')
        ->name('motorcycle_parts_import_from_csv');
  
  Route::prefix('exportar-excel')->group(function(){
    Route::get('/todo', 'MotorcyclePartsController@excel_export_all')
        ->name('motorcycle_parts_excel_export_all')
        ->middleware('role:motorcycle_part__excel_export');
  });


});


Route::prefix('ventas')->group(function () {
  Route::get('/', 'SalesController@index')->name('sales');
  Route::get('/nuevo', 'SalesController@add')->name('sales_new');
  Route::get('/{id}', 'SalesController@show')->name('sales_show')->where('id', '\d+');
  Route::get('/imprimir/{id}', 'SalesController@printing_mode')->name('sales_printing_mode')->where('id', '\d+');
  Route::post('/crear', 'SalesController@create')->name('sales_create');
  Route::put('/{id}', 'SalesController@update')->name('sales_update')->where('id', '\d+');
  Route::get('/reportes-excel', 'SalesController@excel_report')->name('sales_excel_report');
  Route::post('/eliminar/{id}', 'SalesController@delete')->name('sales_delete')->where('id', '\d+');




  Route::prefix('tipos')->group(function () {
    Route::get('/', 'SaleTypesController@index')->name('sale_types');
    Route::post('/', 'SaleTypesController@create')->name('sale_types_create');
    Route::put('/{id}', 'SaleTypesController@update')->name('sale_types_update')->where('id', '\d+');
    Route::delete('/{id}', 'SaleTypesController@delete')->name('sale_types_delete')->where('id', '\d+');

  });


});



Route::group(['prefix' => 'personal-tecnico'], function(){

  Route::get('/', 'TechnicalStaffController@index')
        ->name('technical_staff')
        ->middleware('role:technical_staff__list_records');

  Route::get('/filter_by', 'TechnicalStaffController@filter_by')
        ->name('technical_staff_filter_by');

  Route::get('/nuevo', 'TechnicalStaffController@add')
        ->name('technical_staff_new')
        ->middleware('role:technical_staff__create_record');

  Route::get('/{id}', 'TechnicalStaffController@show')
        ->where('id', '\d+')
        ->name('technical_staff_show')
        ->middleware('role:technical_staff__view_record');
  
  Route::post('/', 'TechnicalStaffController@create')
        ->name('technical_staff_create')
        ->middleware('role:technical_staff__create_record');
  
  Route::post('/{id}', 'TechnicalStaffController@update')
        ->where('id', '\d+')
        ->name('technical_staff_update')
        ->middleware('role:technical_staff__update_record');
  
  Route::post('/eliminar/{id}', 'TechnicalStaffController@delete')
        ->where('id', '\d+')
        ->name('technical_staff_delete')
        ->middleware('role:technical_staff__delete_record');

});




Route::group(['prefix' => 'catalogo-de-productos-de-proveedores'], function(){

  Route::get('/', 'MotorcyclePartsSupplierItemOnSaleController@index')
        ->name('motorcycle_parts_supplier_item_on_sales');

  Route::get('/list', 'MotorcyclePartsSupplierItemOnSaleController@list');

  Route::get('/{id}', 'MotorcyclePartsSupplierItemOnSaleController@show')
        ->where('id', '\d+')
        ->name('motorcycle_parts_supplier_item_on_sales_show');
  
  Route::post('/', 'MotorcyclePartsSupplierItemOnSaleController@create')
        ->name('motorcycle_parts_supplier_item_on_sales_create');
  
  Route::post('/{id}', 'MotorcyclePartsSupplierItemOnSaleController@update')
        ->where('id', '\d+')
        ->name('motorcycle_parts_supplier_item_on_sales_update');
  
  Route::delete('/{id}', 'MotorcyclePartsSupplierItemOnSaleController@delete')
        ->where('id', '\d+')
        ->name('motorcycle_parts_supplier_item_on_sales_delete');

});


Route::group(['prefix' => 'marcas-de-repuestos'], function(){

  Route::get('/', 'MotorcyclePartsBrandController@index')
        ->name('motorcycle_parts_brands')
        ->middleware('role:motorcycle_parts_brand_list_records');

  Route::get('/list', 'MotorcyclePartsBrandController@list');

  Route::get('/{id}', 'MotorcyclePartsBrandController@show')
        ->where('id', '\d+')
        ->name('motorcycle_parts_brands_show')
        ->middleware('role:motorcycle_parts_brand_view_record');
  
  Route::post('/', 'MotorcyclePartsBrandController@create')
        ->name('motorcycle_parts_brands_create')
        ->middleware('role:motorcycle_parts_brand_create_record');
  
  Route::post('/{id}', 'MotorcyclePartsBrandController@update')
        ->where('id', '\d+')
        ->name('motorcycle_parts_brands_update')
        ->middleware('role:motorcycle_parts_brand_update_record');
  
  Route::delete('/{id}', 'MotorcyclePartsBrandController@delete')
        ->where('id', '\d+')
        ->name('motorcycle_parts_brands_delete')
        ->middleware('role:motorcycle_parts_brand_delete_record');

});
